import sys
from os import path

from setuptools import find_packages, setup

import versioneer

setup(name='emiltools',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='A collection of tools and plans specific to EMIL but shared across profiles',
      url='https://gitlab.helmholtz-berlin.de/sissy/experiment-control/emiltools',
      author='Will Smith, Simone Vadilonga, Sebastian Kazarski',
      author_email='william.smith@helmholtz-berlin.de',
      # license='MIT',
      packages=find_packages(exclude=['docs', 'tests']),
      install_requires=[
          'ophyd',
          'numpy'
      ]
      # zip_safe=False
)