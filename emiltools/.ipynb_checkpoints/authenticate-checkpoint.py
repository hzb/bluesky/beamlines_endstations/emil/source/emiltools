#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 25 14:00:22 2022

@author: micha
"""

import icat.config
import icat.client
from icat.query import Query

from ACarchive.ACarchive import ACarchive
icat.config.defaultsection = 'default'
from emiltools.logger import log
from getpass import getpass


def requestInvestigationNames(username, password):
    
    """
    
    A function which authenticates against the ICAT and get's investigations. 
    Aims to be used inside other functions in both GUI and IPython environment. 
    Does not ask for input or present anything in shell, that is for other calling functions to perform
    
    
    Arguments
    ---------
    
    username: string
    password: string
    
    Returns
    ---------
    
    icat_dict: Dict 
         Icat sessionid, icat url, icat investigation id and name.
    
    example: 
    
    {'icat': {'idsurl': 'https://icat.helmholtz-berlin.de',
          'sessionId': adfba0ef-878e-418f-8ab5-7482750fa23d,
          'url': 'https://icat.helmholtz-berlin.de'}}
          
    investigations: Dict
        with keys of the investigation title and items of another dict with the name and id of that title
    
    example: 
    
    {'CPMU17_EMIL Beamline Commissioning 2021/2022':{'id': '7923', 'name': 'misc:21-000008'},
     'METRIXS Commissioning 2022': {'id': '7933', 'name': 'misc:22-000016'},
     'PM4 Beamline Commissioning 2021/2022': {'id': '7924', 'name': 'misc:21-000009'},
     'Test: ingest for SISSY': {'id': '7645', 'name': 'gate:202-00003-1.1-P'},
     'UE48 Beamline Commissioning 2021': {'id': '7874', 'name': 'misc:21-000005'},
     'UE52_SGM Commissioning 2022': {'id': '7935', 'name': 'misc:22-000017'}}


    
    
    
    """
    
    credentials = { "username": username,"password": password}
    auth = 'hzbrex'
    url = 'https://icat.helmholtz-berlin.de'
                
    try:
        client = icat.client.Client(url,idsurl=url)
        client.login(auth, credentials)
        
        
        try:
            log.info(f"ICAT: Created client for user {client.getUserName()}")
            query = Query(client, "Investigation", conditions={
                    "investigationUsers.user.name": "= :user"
                    }, aggregate="DISTINCT",includes=["investigationUsers.user"]) 
            investigations_query_response = client.search(query)
            investigations = {}
            for item in investigations_query_response:
                    investigations[str(item.title)] = {'name': str(item.name), 'id': str(item.id)}
                                        

            return {"icat":{"url":url,"idsurl":url,"sessionId":client.sessionId}}, investigations
        except Exception as e:
            log.error("ICAT: Authentication to Icat failed, Error: ",e)
            raise Exception
        return {"icat":{"url":url,"idsurl":url,"sessionId":client.sessionId}}, None
    except Exception as e:
        log.error("Icat: No Valid SessionID generated, Error: ",e)
    return {}
    
    
    
def loginIcat():
    """
    Reads the credentials from icat.cfg and promts for missing credentials

    Returns
    -------
    Dict contianing: Icat sessionid, icat url, icat investigation id and name.

    """

    
    username =  str(input("username: "))
    password = str(getpass("password: "))
    
    icat_dict, investigations = requestInvestigationNames(username,password)

    for item in investigations:
               print(str(investigations[item]['id']) + "\t" +  str(investigations[item]['name']) + "\t" + str(item) )

    return icat_dict

def loginACarchive(url = 'https://ac.archive.fhi.mpg.de/'):
    """
    Reads the credentials from AC.cfg and promts for missing credentials

    Returns
    -------
    Dictionary containining: ACarchive token, ACarchive url, and poroject name.

    """
    log.info("AC Archive: Authenticating ACreceiver")
    project = "Catlab"
    try: 
        session = ACarchive(url)
        log.info("AC Archive: Session created")
        if session.valid_token:
            return {"ACarchive":{"url":url,"token":session.token,"project":project}}

        else: 
            log.error( "AC ARCHIVE: No Valid Token generated")
    except: 
       log.error("AC ARCHIVE: No Valid Token generated")
    return {}