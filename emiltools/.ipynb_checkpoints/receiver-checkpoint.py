#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 07:32:29 2022

@author: a2853
"""
import os

import emiltools.authenticate as auth
from emiltools.prevac2 import prevacDB
from emiltools.ACarchive import ACarchive
import icat.client
import icat.config
from icat.query import Query
icat.config.defaultsection = 'default'

from emiltools.logger import log

class receiver():
    """
    This is the base class for each receiver for NeXus files. It defines the must implement functions.
    """
    def __init__(self):
        self.credentials = {}
        self.data = {}
        self.local_sample_id =None
        
    def checkSampleExists(self):
        return False
    
    def collectMetaData(self,doc):
        pass
    
    def sent(self,fname):
        pass
    
class ACreceiver(receiver):
    """
    Overwrites receiver. Defines the functionality to sent data to ACarchive.
    """
    def __init__(self,_local_sample_id = None,_md_AC_sample = {}):
        super().__init__()
        self.sampleID = [] # AC archive sampleIDs
        self.md_AC_sample = _md_AC_sample
        # create session to AC archive
        try: 
            self.credentials = auth.loginACarchive()
            ACcredentials = self.credentials["AC"]
            self.session = ACarchive.reinit(ACcredentials['url'],ACcredentials['token'],ACcredentials['project'])
            if not self.session.valid_token:
                log.error("AC archive: Could not establish connection to the ACarchive, did not get a valid token")
                raise ConnectionError
        except Exception as e:
            log.error("AC archive: Authentication to AC archive failed, Error: ",e)
        # check if given sample already exists
        if _local_sample_id:
            self.local_sample_id = str(_local_sample_id) #local sample ID, prevac
            try:
                if not self.checkSampleExists():
                    #create sample with data from  prevac database
                    entryID = self.session.createEntry(self.md_AC_sample,_entrytype='sample')
                    self.session.jsonupdate({'emil_sample_id':self.local_sample_id},entryID)
                    self.sampleID.append(entryID)

            except Exception as e:
                log.error("AC archive: could not check if sample exists, Error: ",e)
      
        
    
    def checkSampleExists(self):
        try:
            result = self.session.jsonsearch('emil_sample_id')
            count = 0
            for key in result.keys():
                emil_sample_id = result[key]['emil_sample_id']
                if str(emil_sample_id) == self.local_sample_id:
                    self.sampleID.append( key)
                    count +=1
            if count ==1:
                log.info(f"AC archive: Sample {self.local_sample_id} was found on the AC archive once ID {self.sampleID}")#
                return True
            elif count > 1:
                log.info(f"AC archive: Sample {self.local_sample_id} was found on the AC archive more than once, IDs {self.fsampleID}")
                return True
            else:
                log.info(f"AC archive: Sample {self.local_sample_id} was not found on the AC archive ")
                return False
        except Exception as e:
            log.error("AC archive: Failed to check if sample exists, Error: ",e)
        
    def collectMetaData(self,doc):
        try:
            fields = self.session.getFields('data') 
            md_AC = doc if "md_AC" not in doc else doc['md_AC']
            for field in fields:
                field2 = field.replace(" ","")
                if field == "Yield (%)":
                    field2 = "Yield"
                self.data[field] = md_AC[field2] if field2 in md_AC else ''
        except Exception as e:
            log.error("AC ARCHIVE: Collection of Meta Data from Bluesky Document failed, Connection to ACarchive failed",e)
 
    def sent(self,fname):
        try:
            entryID = self.session.createEntry(self.data,_entrytype='data')
            self.session.uploadNexusFileBase64(entryID,fname,os.path.basename(fname))            
            if len(self.sampleID) > 0:
                for i in self.sampleID:
                    self.session.addLinks(i,[entryID])
        except:
            log.error("AC ARCHIVE: Upload of data to ACarchive data object failed")
     

class icatreceiver(receiver):
    """
    Overwrites receiver. Defines the functionality to sent data to HZB icat.
    """
    def __init__(self,_inv=None):
        super().__init__()
        try:
            self.credentials = auth.loginIcat(_inv)
            iccredentials = self.credentials["icat"]
            self.client = icat.client.Client(iccredentials["url"],idsurl=iccredentials["idsurl"])
            self.client.sessionId = iccredentials["sessionid"]
            query = Query(self.client, "Investigation", conditions={"id": "= '"+str(iccredentials["investigationid"])+"'"})
            self.investigation = self.client.assertedSearch(query)[0]
            log.info(f"ICAT: Created client for user {self.client.getUserName()}")
        except Exception as e:
            log.error("ICAT: Authentication to Icat failed, Error: ",e)
        
    
    def sent(self,fname):    
        fname_base = os.path.basename(fname) 
        try:
            dataset = self.client.new("dataset")
            dataset.investigation = self.investigation
            dataset.type = self.client.assertedSearch(Query(self.client, "DatasetType", conditions={"name": "= 'other'"}))[0]
            dataset.name = fname_base
            dataset.complete = False
            dataset.create()
            log.info("ICAT: created dataset")
        except  icat.exception.ICATObjectExistsError:
            dataset = self.client.search(Query(self.client,"Dataset", conditions={"name": "= '"+fname_base+"'"}))[0]
            log.info("ICAT: loaded dataset")
        except Exception as e:
            log.error("ICAT: Could not connect to icat",e)
        try: 
            df_format = self.client.assertedSearch(Query(self.client, "DatafileFormat", conditions={"name": "= 'NeXus'"}))[0]
            datafile = self.client.new("datafile", name=fname_base, dataset=dataset, datafileFormat=df_format)
            self.client.putData(fname, datafile)
            log.info("ICAT: Uploaded datafile")
        except Exception as e:
            log.error("ICAT: Upload of data to icat failed",e)
