from __future__ import with_statement

from apstools.callbacks.callback_base import FileWriterCallbackBase 
from apstools.callbacks.nexus_writer import NXWriter
from bluesky.callbacks.zmq import RemoteDispatcher
import time
import databroker
from datetime import datetime, timezone
import h5py
from emiltools.logger import log as logger
import numpy as np
import os
import yaml
import argparse
from bluesky.callbacks.zmq import Proxy




NEXUS_FILE_EXTENSION = "hdf"  # use this file extension for the output
NEXUS_RELEASE = "v2020.1"  # NeXus release to which this file is written

class NXWriterBESSYXAS(NXWriter):
    """
    BESSYII X-Ray Source Specific class for writing HDF5/NeXus file (using APS NXWriter class).
    
    Conforms to NXArchiver Standard.
    
    Methods of NXWriter are overwritten to make things specific to BESSY

    One scan is written to one HDF5/NeXus file at location specified by self.file_path with name
    given by self.make_file_name
    
    EXAMPLE write a run from a catalog
    
    nxwriter = NXWriter()
    nxwriter.file_path  = os.getcwd() # This is also the default
    nxwriter.instrument_name = 'SISSY1' 
    run = db[-1]
    nxwriter.export_run(run)
    
    EXAMPLE subscribe a RE 
    
    RE.subscribe(nxwriter.receiver)

    METHODS

    .. autosummary::

       ~export_run
       ~get_values
       ~make_file_name
       ~start
       ~write_absorbed_beam
       ~write_entry
       ~write_incoming_beam
       ~write_instrument
       ~write_monochromator
       ~write_sample
       ~write_source
       ~write_user
     
    """
    # convention: methods written in alphabetical order
    def __init__(self,_receivers=[]):
        super().__init__()
        self.data = {} # dictionary to store information from start doc
        self.session= None
        self.receivers = _receivers
        
        
        
    def export_run(self,run):

        """
        export a single run from a catalog to a single file
        """
        if isinstance(run, databroker.core.BlueskyRun):
            h = run.catalog_object.v1[run.name]  # header
            for key,doc in h.db.get_documents(h):
                self.receiver(key, doc)
        else:
            raise ValueError(f'{run}: is not an instance of databroker.core.BlueskyRun')



    def get_values(self,parent,signal,stream="primary"):
        uid0 = self.streams[stream][0]  # just get the one descriptor uid
        acquisition = self.acquisitions[uid0]
        v = acquisition["data"][signal]
        d = v["data"]
        if isinstance(d, list) and len(d) > 0:
            if v["dtype"] in ("string",):
                d = self.h5string(d)
            elif v["dtype"] in ("integer", "number"):
                d = np.array(d)
        try:
            name = "data"
            if stream == "baseline":
                name += "_" + signal
            ds = parent.create_dataset(name, data=d)
            ds.attrs["target"] = ds.name
        except TypeError as exc:
            logger.error("%s %s %s %s",v["dtype"],signal,f"TypeError({exc})",v["data"],)

        
    def make_file_name(self):
        """
        generate a file name to be used
        
        """
        date_string = datetime.now().strftime('%Y_%m_%d')
        fname = f"{self.scan_id:05d}"
        fname += f"_{self.uid[:7]}.{self.file_extension}"
        path = os.path.abspath(self.file_path or os.getcwd())
        path = os.path.join(path,date_string)
        path = os.path.join(path,'nx')
        os.makedirs(path, exist_ok=True)
        return os.path.join(path, fname)
    
    def start(self,doc):
        """
        Extend start function fromparentclass callback_base.py to capture
        diffenert writing logic
        """
        super().start(doc)
        for receiver in self.receivers:
            receiver.collectMetaData(doc)

   
        
    def writer(self):
        """
        write collected data to HDF5/NeXus data file
        """
        fname = self.file_name or self.make_file_name()
        with h5py.File(fname, "w") as self.root:
            self.write_root(fname)
           
        self.root = None
        logger.info(f"wrote NeXus file: {fname}")  # lgtm [py/clear-text-logging-sensitive-data]
        self.output_nexus_file = fname
        for receiver in self.receivers:
            receiver.sent(fname)
   
    def write_absorbed_beam(self, parent):
        """
        group: /entry/instrument/absorbed_beam:NXdetector
        """
        nxabsorbedbeam = self.create_NX_group(
            parent, "absorbed_beam:NXdetector"
        )
        self.get_values(nxabsorbedbeam,self.metadata["absorbed_beam"])
        return nxabsorbedbeam    
    
    def write_baseline(self,parent):
        """
        group: /entry/baseline:NXnote
        """
        acquisition = self.acquisitions[self.streams["baseline"][0]]
        nxbaseline = self.create_NX_group(parent,"baseline:NXcollection")
        for signal in acquisition["data"]:
            self.get_values(nxbaseline,signal,"baseline")
        nxbaseline.attrs["description"] = "First value is the detector read at start, second value at end"
        return nxbaseline
        
    
    def write_data(self, parent):
        """
        group: /entry/data:NXdata
        """
        nxdata = self.create_NX_group(parent, "data:NXdata")

        nxdata["energy"] = parent["instrument/monochromator/data"]
        nxdata["absorbed_beam"] = parent["instrument/absorbed_beam/data"]
        
        nxdata.attrs["axes"] = "energy"
        nxdata.attrs["signal"] = "absorbed_beam"
        
        return nxdata
        
    def write_entry(self):
        """
        group: /entry/data:NXentry
        """
        nxentry = self.create_NX_group(
            self.root, self.root.attrs["default"] + ":NXentry"
        )

        nxentry.create_dataset(
            "start_time",
            data=datetime.fromtimestamp(
                self.start_time
            ).isoformat(),
        )
        nxentry.create_dataset(
            "end_time",
            data=datetime.fromtimestamp(
                self.stop_time
            ).isoformat(),
        )
        
        nxentry.create_dataset(
            "definition",
            data='NXxas',
        )

        nxentry.create_dataset("program_name", data="bluesky")
        self.write_baseline(nxentry)

        self.write_instrument(nxentry)  # also writes streams and metadata
        self.write_data(nxentry)
       
        self.write_sample(nxentry)
        self.write_user(nxentry)

        nxentry["title"] = self.get_sample_title()
        nxentry["plan_name"] = self.metadata["plan_name"]
        #nxentry["entry_identifier"] = self.streams["primary"][0] 
        if "investigation_id" in self.metadata:
            nxentry["investigation_id"] = self.metadata["investigation_id"]
        

        return nxentry
        
    
   

    
    def write_incoming_beam(self, parent):
        """
        group: /entry/instrument/incoming_beam:NXdetector
        """
        nxincomingbeam = self.create_NX_group(
            parent, "incoming_beam:NXdetector"
        )
        self.get_values(nxincomingbeam,self.metadata["incoming_beam"])

        return nxincomingbeam

    

    
               
    def write_instrument(self, parent):
        """
        group: /entry/instrument:NXinstrument
        """
        nxinstrument = self.create_NX_group(
            parent, "instrument:NXinstrument"
        )
        
        self.write_source(nxinstrument)

        self.write_monochromator(nxinstrument)
        self.write_incoming_beam(nxinstrument)
        self.write_absorbed_beam(nxinstrument)
        
        return nxinstrument
    
    
    def write_monochromator(self, parent):
        """
        group: /entry/instrument/monochromator:NXmonochromator
        """           
        nxmonochromator = self.create_NX_group(
            parent, "monochromator:NXmonochromator"
        )
        self.get_values(nxmonochromator,self.metadata["monochromator"])

        return nxmonochromator
    
    def write_sample(self, parent):
        """
        group: /entry/sample:NXsample
        """
        
        sample = None
        try:
            sample = self.metadata['sample']
           
        except KeyError as exc:
            logger.warning("no %s defined -- not creating sample group", str(exc))
            return

        nxsample = self.create_NX_group(parent, "sample:NXsample")
        
        if isinstance(sample, dict):
                for k, v in sample.items():
                    nxsample[k] = v
        elif isinstance(sample, str):
                nxsample.create_dataset("name", data=sample)
        

        return nxsample
    
    def write_source(self, parent):
        """
        group: /entry/instrument/source:NXsource

        """
        nxsource = self.create_NX_group(parent, "source:NXsource")

        nxsource.create_dataset("name", data="BESSY II")
        nxsource.create_dataset("type", data="Synchrotron X-Ray Source")
        nxsource.create_dataset("probe", data="x-ray")

        return nxsource
    
                        
    
    def write_user(self, parent):
        """
        group: /entry/contact:NXuser
        """
        keymap = dict(
            name='user_name',
            facility_user_id='user_profile'
        )

        try:
            links = {k: self.metadata[v] for k, v in keymap.items()}
        except KeyError as exc:
            logger.warning("no %s defined -- not creating user group", str(exc))
            return
        
        
        nxuser = self.create_NX_group(parent, "user:NXuser")
        nxuser.create_dataset("role", data="contact")
        for k, v in links.items():
            nxuser[k] = v
        
        
        return nxuser

def start_proxy(return_dict):
    
    proxy = Proxy()
    return_dict['in_port']=proxy.in_port
    return_dict['out_port']=proxy.out_port
    proxy.start()

def start_publisher(out_port,file_path,receivers=[]):
    
    
    d = RemoteDispatcher('localhost:'+ str(out_port))
    nxwriter = NXWriterBESSYXAS(_receivers=receivers)
    os.makedirs(file_path, exist_ok=True)
    nxwriter.file_path = file_path
    d.subscribe(nxwriter.receiver)
    # when done subscribing things and ready to use:
    d.start()  # runs event loop forever
