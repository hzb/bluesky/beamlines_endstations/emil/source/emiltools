import logging
import nextcloud_client
import os
from datetime import datetime


import time
MAX_ATTEMPTS = 360


logger = logging.getLogger("emiltools")


def file_exists(nc,path):
    
    attempts = 0
    got_answer = False
    while attempts<MAX_ATTEMPTS and got_answer == False:
        
        try:
            nc.file_info(path)
            got_answer = True
            return True #The file exists
        except Exception as error:
            
            if "404" in str(error):
                got_answer = True
                return False        #The file does not exists
            else:
                logger.error(f"error code {error} when attempting to find if path {path} exists after {attempts}")
                got_answer = False # We don't know if the file exists, carry on trying
                attempts = attempts + 1

        time.sleep(20)

def mkdir(nc, path):
    
    if file_exists(nc, path):
        #the path already exists return
        return None
    
    else:
        #attempt to make the new directory

        attempts = 0
        dir_made = False
        while attempts<MAX_ATTEMPTS and dir_made == False:
        
            try:
                nc.mkdir(path)
                logger.info(f"successfully made new dir {path} after {attempts} attempts")  
                dir_made = True
                return None
            except Exception as error:
                        
                logger.error(f"error code {error} when attempting to make new dir {path} after attempts {attempts}")
                attempts = attempts +1
                dir_made = False       

            time.sleep(20)    



def export(nc, path, file):
    

    # First make the path if it doesn't exist on nubes
    split_path = path.split('/')[:-1]    
    old_path = ''
    for dir_str in split_path:

        if not file_exists(nc,old_path+'/'+dir_str):

            mkdir(nc,old_path+'/'+dir_str)
            

        old_path = old_path +'/'+dir_str

    
    if os.path.exists(file):
        uploaded = False
        attempts = 0
        while not uploaded and attempts < MAX_ATTEMPTS:
            try:
                nc.put_file(path,file)
                logger.info(f"uploaded {file} to {path} after {attempts}")  
                uploaded = True
            except Exception as error:
                logger.error(f"error code {error} when uploading {file} to {path}, attempt {attempts}")
                attempts = attempts +1

                time.sleep(20)

        if not uploaded and attempts >= MAX_ATTEMPTS:
            logger.error(f"Failure.{file} was not uploaded even after {attempts} attempts")


    else:
            logger.error(f"file {file} doesn't exist so can't upload")

