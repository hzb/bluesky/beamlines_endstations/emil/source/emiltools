#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os

#from emiltools.prevac2 import prevacDB
from ACarchive.ACarchive import ACarchive
import icat.client
import icat.config
from icat.query import Query
icat.config.defaultsection = 'default'
import psycopg2
import logging
import h5py
import numpy
from getpass import getpass
import nextcloud_client
from .nubes_upload import export
from datetime import datetime

logger = logging.getLogger("emiltools")

class receiver():
    """
    This is the base class for each receiver for NeXus files. It defines the must implement functions.
    """
    def __init__(self):
        pass
    
    def collectMetaData(self,doc):
        """
        Function which is called when the bluesky start document is passed
        
        Arguments
        ---------
        
        doc: bluesky start document
        """
        pass
    
    def sent(self,fname):
        """
        Function which is called when the nexus writer in the bluesky end document is called, 
        after the writing process is finished, sents the nexus file to the intended archive 
        
        Arguments
        ---------
        
        fname: name of the created nexus file to be processed
        """
        pass


class Nubesreceiver(receiver):
    """
    Overwrites receiver. Defines the functionality to sent data to ACarchive.
    """
    def __init__(self):
        super().__init__()
        self.credentials = {}

    def collectMetaData(self,doc):
        """
        Function which is called when the bluesky start document is passed
        collects the metadata so that we can authenticate and send the required files to the right location
        
        Arguments
        ---------
        
        doc: bluesky start document
        """
        if "Nubes" in doc:
            self.credentials = doc["Nubes"]
        else:
            logger.error("Start Doc. There is no information in the md dict to export any files to Nubes")

    def sent(self, fname):
        """
        Function which is called when the nexus writer in the bluesky end document is called, 
        after the writing process is finished, this method is used to send the files to the location specified in credentials
        
        Arguments
        ---------
        
        fname: name of the created file to be processed
        """
        if "public_link" in self.credentials and 'public_pass' in self.credentials:
            try:
                nc = nextcloud_client.Client.from_public_link(self.credentials['public_link'], folder_password = self.credentials['public_pass']) 

                try:
                    filepath, filename = os.path.split(fname)
                    date_string = datetime.now().strftime('%Y_%m_%d')
                    export(nc,self.credentials['nubes_path']+"/"+date_string+"/"+filename,fname)
                except Exception as e:

                    logger.error(f"Nubes Export: Error exporting {fname} with error: {e}")

            except Exception as e:

                logger.error("Nubes Export: Error connecting to Nubes with credentials",e)
        else:
            logger.error("Trying to send to Nubes. There is no information in the md dict to export any files to Nubes")



class ACreceiver(receiver):
    """
    Overwrites receiver. Defines the functionality to sent data to ACarchive.
    """
    def __init__(self):
        super().__init__()
        self.data = {}
   
    
    def collectMetaData(self,doc):
        """
        Function which is called when the bluesky start document is passed
        collects the meta data for the AC Archive standard fields, depending
        if a sample or an data entry is created
        reads the ACarchive credentials from the document

        
        Arguments
        ---------
        
        doc: bluesky start document
        """
        self.credentials = doc["ACarchive"]
        try:
            session = ACarchive.reinit(self.credentials["url"], \
                                       self.credentials["token"], self.credentials["project"])
            md_AC = doc if "md_AC" not in doc else doc['md_AC']
            print(md_AC)
            self.entrytype = md_AC["entrytype"] if "entrytype" in md_AC else "data"
            fields = session.getFields(self.entrytype) 

            for field in fields:
                field2 = field.replace(" ","")
                if field == "Yield (%)":
                    field2 = "Yield"
                self.data[field] = md_AC[field2] if field2 in md_AC else ''
        except Exception as e:
            logger.error("AC ARCHIVE: Collection of Meta Data from Bluesky Document failed, Connection to ACarchive failed",e)
 
    def sent(self,fname):
        """
        Function which is called when the nexus writer in the bluesky end document is called, 
        after the writing process is finished, sents the nexus file to the AC Archive 
        
        Arguments
        ---------
        
        fname: name of the created nexus file to be processed
        """
        try:
            session = ACarchive.reinit(self.credentials["url"], \
                           self.credentials["token"], self.credentials["project"])            
            entryID = session.createEntry(self.data,_entrytype=self.entrytype)
            session.uploadBase64(entryID,fname,os.path.basename(fname))            
        except Exception as e:
            logger.error("AC ARCHIVE: Upload of data to ACarchive data object failed, Error: ",e)
     

class icatreceiver(receiver):
    """
    Overwrites receiver. Defines the functionality to sent data to HZB icat.
    """
    def __init__(self):
        super().__init__()
        self.credentials = {}
        self.investigation = None
        
        
    def updateInvestigation(self,_inv_id):
        """
        Function which sets the investigation id, it creates a client and check 
        if the investigation id is valid and can be used with this icat
        credentials, throws an error if not valid
        
        Arguments
        ---------
        
        _inv_id: string, investigation id
        """
        logger.info("Updating Investigation ID")
        if self.investigation and int(self.investigation.id) == int(_inv_id):
            return
        try:
            client = icat.client.Client(self.credentials["url"],idsurl=self.credentials["idsurl"])           
            client.sessionId = self.credentials["sessionId"]
            query = Query(client, "Investigation", conditions={"id": "= '"+str(_inv_id)+"'"})
            self.investigation = client.assertedSearch(query)[0]
        except Exception as e:
            logger.error("Could not change investigation, Error: ",e)
    
    def collectMetaData(self,doc):
        """
        Function which is called when the bluesky start document is passed
        searches the doc for the investigation id and sets/updates the investigation id
        reads the icat credentials from the document
        
        Arguments
        ---------
        
        doc: bluesky start document
        """
        if "icat" in doc:

            self.credentials = doc["icat"]
            if "investigationid" in doc:
                self.updateInvestigation(doc["investigationid"])
        else:
            logger.error("Start Doc. There is no information on icat, even though the reciever is in the recievers list")
    
    def sent(self,fname):  
        """
        Function which is called when the nexus writer in the bluesky end document is called, 
        first it scans  the nexus files for valid icat meta data, i.e. by going through all
        Parametertypes and checking if they are present in the nexus file
        after the writing process is finished, sents the nexus file to the icat
        
        TODO: The writing process is done over http right now, should be changed to samba in the future
        
        Arguments
        ---------
        
        fname: name of the created nexus file to be processed
        """
        fname_base = os.path.basename(fname) 
        if "url" in self.credentials and "idsurl" in self.credentials and "sessionId" in self.credentials:
            try:
                client = icat.client.Client(self.credentials["url"],idsurl=self.credentials["idsurl"])           
                client.sessionId = self.credentials["sessionId"]

                dataset = client.new("dataset")
                dataset.investigation = self.investigation
                dataset.type = client.assertedSearch(Query(client, "DatasetType", conditions={"name": "= 'other'"}))[0]
                dataset.name = fname_base
                dataset.complete = False
                
                def createParameters(name, obj):
                    if isinstance(obj,h5py.Dataset):
                        query = Query(client, "ParameterType", conditions={"name": "= 'nxs/"+str(name)+"'" })
                        try:
                            paramtype = client.assertedSearch(query)[0]
                            if paramtype.valueType == "STRING":
                                dsparam = client.new("datasetParameter",type=paramtype, stringValue=obj[()].decode())
                            else :
                                if isinstance(obj[()],float) or  isinstance(obj[()],int):
                                    dsparam = client.new("datasetParameter",type=paramtype, numericValue=obj[()],rangeBottom=obj[()],rangeTop=obj[()])
                                if isinstance(obj[()],numpy.ndarray):
                                    dsparam = client.new("datasetParameter",type=paramtype, numericValue=numpy.mean(obj[()]),rangeBottom=numpy.min(obj[()]),rangeTop=numpy.max(obj[()]))

                            dataset.parameters.append(dsparam)
                        except:
                            return None
                    
                    return None
                # adding metadata to parameters to browse data in the icat
                with h5py.File(fname, "r")  as f: 
                    f.visititems(createParameters) 
                logger.debug(dataset)
                dataset.create()
                logger.info("ICAT: created dataset")
            except  icat.exception.ICATObjectExistsError:
                dataset = client.search(Query(client,"Dataset", conditions={"name": "= '"+fname_base+"'"}))[0]
                logger.info("ICAT: loaded dataset")
            except Exception as e:
                logger.error("ICAT: Could not connect to icat",e)

            try: 
                df_format = client.assertedSearch(Query(client, "DatafileFormat", conditions={"name": "= 'NeXus'"}))[0]
                datafile = client.new("datafile", name=fname_base, dataset=dataset, datafileFormat=df_format)
                client.putData(fname, datafile)
                logger.info("ICAT: Uploaded datafile")
            except Exception as e:
                logger.error("ICAT: Upload of data to icat failed",e)
        else:
            logger.error("ICAT Upload: There is no credential information to upload data")
