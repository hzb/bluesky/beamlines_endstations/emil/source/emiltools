import pytest
import logging
from emiltools.log import config_emiltools_logging
import os

logfile = str(os.getcwd())+"/test.log"
config_emiltools_logging(file=logfile, level="DEBUG", maxBytes=2000000000)

logger = logging.getLogger("emiltools")

def check_in_file(file, string):

    with open(file) as f:
        if string in f.read():
            return True
        else:
            return False
             

def test_log_file_writing():


    message = "This is an info message"
    logger.info(message)
    assert os.path.isfile(logfile) == True

    assert check_in_file(logfile, message) == True
    
    os.remove(logfile)


    
