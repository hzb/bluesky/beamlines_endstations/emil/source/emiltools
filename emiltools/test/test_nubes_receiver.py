from emiltools.receiver import Nubesreceiver

import pytest
from emiltools.nubes_upload import export, file_exists
import nextcloud_client

public_link = 'https://nubes.helmholtz-berlin.de/s/MJ6TKqDWG6iF9rD'
public_pass = 'wqcysMYEmXeK4fN'



def test_receiver():


    nubes_receiver = Nubesreceiver()

    meta_dict = {"public_link":public_link, "public_pass":public_pass, "nubes_path":"NubesTest" }

    nubes_receiver.credentials = meta_dict

    nubes_receiver.sent("Data/test_data.txt")

    nc = nextcloud_client.Client.from_public_link(public_link, folder_password = public_pass) 
    assert file_exists(nc, "NubesTest/test_data.txt") == True
    nc.delete("NubesTest")

