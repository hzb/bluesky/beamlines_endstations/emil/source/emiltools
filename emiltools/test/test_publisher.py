import pytest
import os,sys

from ophyd.sim import motor, noisy_det
from bluesky.preprocessors import SupplementalData
from jinja2 import Template
from bluesky import RunEngine
from databroker.v2 import temp
import databroker
from bluesky import Msg
from datetime import datetime, timezone
from emiltools.publisher import NXWriterBESSY, CSVCallback

import multiprocessing
from multiprocessing import Process
from bluesky.callbacks.zmq import Publisher
import time
## Set up env
from bluesky.plans import scan, list_grid_scan
import h5py
from bluesky.preprocessors import SupplementalData

from emiltools.receiver import ACreceiver, icatreceiver, Nubesreceiver
from ACarchive.ACarchive import ACarchive

from bluesky.plans import scan
from ophyd.sim import SynGauss
from ophyd import Component as Cpt
from ophyd import Signal
import numpy as np
from dotenv import load_dotenv
from emiltools.authenticate import loginIcat


from emiltools.log import config_emiltools_logging
logfile = str(os.getcwd())+"/test.log"
config_emiltools_logging(file=logfile, level="DEBUG")

### implement the tests

def test_writer_file_path_change():
    
    nxwritera = NXWriterBESSY()
    file_path_test = os.path.join(os.getcwd(),'test')
    nxwritera.file_path = file_path_test
    assert nxwritera.file_path == file_path_test
    
def test_nxwriter_structure():
    #check that the writer generates a file with NXArchive Structure https://manual.nexusformat.org/classes/applications/NXarchive.html
    RE = RunEngine({})
    nxwriterb = NXWriterBESSY()
    file_path = os.path.join(os.getcwd(),'test')
    nxwriterb.file_path = file_path
    RE.subscribe(nxwriterb.receiver)
    db = temp()
    RE.subscribe(db.v1.insert)
    
    
    sd = SupplementalData()
    sd.baseline = [noisy_det]
    RE.preprocessors.append(sd)


    #perform a plan with some dummy variables
    RE(scan([noisy_det],motor,-1,1,10),sample={'name':'test_sample', 'sample_id':1234,'description':'my awesome test sample','type':'sample+can', 'chemical_formula':'ABC','situation':'vacuum'},user_name='test_user',user_profile='test_user_profile')
    
    #Once it's complete check that a file exists under
    uid = db[-1].metadata['start']['uid']
    scan_id = db[-1].metadata['start']['scan_id']
    date_string = datetime.now().strftime('%Y_%m_%d')
    fname = f"{scan_id:05d}"
    fname += f"_{uid[:7]}.hdf"
    file = os.path.join(nxwriterb.file_path,date_string,'nx',fname)
    
    # After a short pause, test that the file is created
    time.sleep(2)
    assert os.path.isfile(file)
    
    #Does it have an entry group
    f = h5py.File(file, 'r')
    keys = list(f.keys())
    assert 'entry' in keys
    
    #Does the entry group contain a title, start_time and end_time
    entry_keys = f['entry']
    assert 'title' in entry_keys
    assert 'start_time' in entry_keys
    assert 'end_time' in entry_keys
    assert 'user'in entry_keys
    assert 'sample'in entry_keys
    assert 'instrument'in entry_keys
    assert 'data'in entry_keys
    
    #Does the user group contain the name, role and facility id?
    entry_user_keys = f['entry']['user']
    assert entry_user_keys.attrs['NX_class'] == 'NXuser'
    assert 'name' in entry_user_keys
    assert entry_user_keys['name'][()].decode('UTF-8') == 'test_user'
    assert 'facility_user_id' in entry_user_keys
    assert entry_user_keys['facility_user_id'][()].decode('UTF-8') == 'test_user_profile'
    assert 'role' in entry_user_keys
    assert entry_user_keys['role'][()].decode('UTF-8') == 'contact'
    
    #Does the sample group contain the sample information
    entry_sample_keys = f['entry']['sample']
    assert entry_sample_keys.attrs['NX_class'] == 'NXsample'
    assert 'name' in entry_sample_keys
    assert entry_sample_keys['name'][()].decode('UTF-8') == 'test_sample'
    assert 'sample_id' in entry_sample_keys
    assert entry_sample_keys['sample_id'][()] == 1234
    
    #Does the data group contain the data?  
    entry_data_keys = f['entry']['data']
    assert len(entry_data_keys['noisy_det']) == 10
    
    #Does the instrument contain source?
    entry_instrument_keys = f['entry']['instrument']
    assert entry_instrument_keys.attrs['NX_class'] == 'NXinstrument'
    assert 'source' in entry_instrument_keys
    
    #Does the source contain type, name, and probe?
    entry_source_keys = f['entry']['instrument']['source']
    assert entry_source_keys.attrs['NX_class'] == 'NXsource'
    assert 'type' in entry_source_keys
    assert entry_source_keys['type'][()].decode('UTF-8') == 'Synchrotron X-Ray Source'
    assert 'name' in entry_source_keys
    assert entry_source_keys['name'][()].decode('UTF-8') == 'BESSY II'
    assert 'probe' in entry_source_keys
    assert entry_source_keys['probe'][()].decode('UTF-8') == 'x-ray'
    
    #Does the instrument contain the baseline?
    assert 'baseline' in entry_instrument_keys['bluesky']['streams']
    assert 'noisy_det' in entry_instrument_keys['bluesky']['streams']['baseline']

def test_from_catalog():
    #https://github.com/BCDA-APS/apstools/blob/main/docs/source/resources/demo_specfile_example.ipynb

    #Create temp broker
    RE = RunEngine({})
    db = temp()
    RE.subscribe(db.v1.insert)
    
    sd = SupplementalData()
    sd.baseline = [noisy_det]
    RE.preprocessors.append(sd)
    
    #Generate some data
    RE(scan([noisy_det],motor,-1,1,10),sample={'name':'test_sample', 'sample_id':1234,'description':'my awesome test sample','type':'sample+can', 'chemical_formula':'ABC','situation':'vacuum'},user_name='test_user',user_profile='test_user_profile')
    
    #Create the writer
    nxwriterc = NXWriterBESSY()
    file_path = os.path.join(os.getcwd(),'test')
    nxwriterc.file_path = file_path

    #Get the run from the catalog
    run = db[-1]
    
    # to get the raw document stream, need the v1 interface
    nxwriterc.export_run(run)
    
    # Once complete, test that the file exists in the correct place
    uid = db[-1].metadata['start']['uid']
    scan_id = db[-1].metadata['start']['scan_id']
    date_string = datetime.now().strftime('%Y_%m_%d')
    fname = f"{scan_id:05d}"
    fname += f"_{uid[:7]}.hdf"
    file = os.path.join(nxwriterc.file_path,date_string,'nx',fname)

    assert os.path.isfile(file)

def test_from_catalog_with_None_in_Meta():
    #https://github.com/BCDA-APS/apstools/blob/main/docs/source/resources/demo_specfile_example.ipynb

    #Create temp broker
    RE = RunEngine({})
    db = temp()
    RE.subscribe(db.v1.insert)
    
    sd = SupplementalData()
    sd.baseline = [noisy_det]
    RE.preprocessors.append(sd)
    
    #Generate some data
    RE(list_grid_scan([noisy_det],motor,[-1,-0.1,0,1]),sample={'name':'test_sample', 'sample_id':1234,'description':'my awesome test sample','type':'sample+can', 'chemical_formula':'ABC','situation':'vacuum'},user_name='test_user',user_profile='test_user_profile')
    
    #Create the writer
    nxwriterc = NXWriterBESSY()
    file_path = os.path.join(os.getcwd(),'test')
    nxwriterc.file_path = file_path

    #Get the run from the catalog
    run = db[-1]
    
    # to get the raw document stream, need the v1 interface
    nxwriterc.export_run(run)
    
    # Once complete, test that the file exists in the correct place
    uid = db[-1].metadata['start']['uid']
    scan_id = db[-1].metadata['start']['scan_id']
    date_string = datetime.now().strftime('%Y_%m_%d')
    fname = f"{scan_id:05d}"
    fname += f"_{uid[:7]}.hdf"
    file = os.path.join(nxwriterc.file_path,date_string,'nx',fname)

    assert os.path.isfile(file)
    
    
    
    
    


@pytest.mark.skip(reason="No AC Archive Creds")
def test_xas_publisher_to_ACarchive():
    import os
    
    from emiltools.publisher_xas import NXWriterBESSYXAS
    
    import emiltools.authenticate as auth
    cred = auth.loginACarchive("https://test.archive.fhi.mpg.de/")

    
    """
    Initialize Runner and databroker
    """
    RE = RunEngine({})
    
    db = temp()
  
    receivers = [ACreceiver()]
    nxwriter = NXWriterBESSYXAS(_receivers=receivers)
    file_path = os.getcwd()
    nxwriter.file_path = file_path
    RE.subscribe(nxwriter.receiver)
    
    # Subscribe our callback
    #Add baseline measurements
    from bluesky.preprocessors import SupplementalData
    
    sd = SupplementalData()
    RE.preprocessors.append(sd)
    
    sd.baseline = [noisy_det,det2]
    md = {"sample":{"name":"test sample","formula":"SiO2"},\
                        "investigationid":'7936',\
                        "user_name":"Micha","user_profile":"a2853"}
    md_AC = {"md_AC":{   "Yield":0.5,\
        "Preparator":'Micha',\
        "Comment":"Pytest ACarchive Micha",\
        "DateofPreparation":'15.02.2022',
        "Elements":"N,O,C",\
        "reason":'Testing',
        "entrytype":"sample"
        }}
        
    md.update(cred)
    md.update(md_AC)
    print(md)
    
    RE(xas(1,2,0.01,md=md))

    
    time.sleep(10)
    entryIDs = session.search(["Pytest ACarchive Micha"])
    assert len(entryIDs) >= 1
    entryID = entryIDs[0]
    
    response = session.getEntry(entryID)
    assert len(response["files"]) == 1
    docid = response["files"][0]["id"]
    file_name = "test.hdf"
    assert session.downloadFile(entryID, docid,file_name) == 1
    assert h5py.is_hdf5(file_name)
    
    for entry in entryIDs:
        assert session.deleteEntry(entry) == 1

#@pytest.mark.skip(reason="This works")
def test_csv_nubes_receiver():

    RE = RunEngine({})

    #create nubes receiver and authenticate
    public_link = 'https://nubes.helmholtz-berlin.de/s/MJ6TKqDWG6iF9rD'
    public_pass = 'wqcysMYEmXeK4fN'
    
    meta_dict = {"public_link":public_link, "public_pass":public_pass, "nubes_path":"NubesTest/" }

    RE.md["Nubes"] = meta_dict #This contains the authorisation and where we want to send the data later


    #create the callback
    csv_publisher = CSVCallback(_receivers = [Nubesreceiver()])
    pub_sub_id=RE.subscribe(csv_publisher.receiver)


    RE(scan([noisy_det],motor,1,2,10))

def test_nx_nubes_receiver():

    RE = RunEngine({})

    #create nubes receiver and authenticate
    public_link = 'https://nubes.helmholtz-berlin.de/s/MJ6TKqDWG6iF9rD'
    public_pass = 'wqcysMYEmXeK4fN'
    
    meta_dict = {"public_link":public_link, "public_pass":public_pass, "nubes_path":"NubesTest/" }

    RE.md["Nubes"] = meta_dict #This contains the authorisation and where we want to send the data later


    #create the callback
    nx_publisher = NXWriterBESSY(_receivers=[ Nubesreceiver()])
    pub_sub_id=RE.subscribe(nx_publisher.receiver)


    RE(scan([noisy_det],motor,1,2,10))

def test_nx_icat_receiver():

    load_dotenv()
    pw = os.environ.get("HZB_PASSWORD")
    username = os.environ.get("HZB_USERNAME")

    RE = RunEngine({})

    icat_dict = loginIcat(username, pw)
    
    md = {"sample":{"name":"test sample","formula":"SiO2"},\
                        "investigationid":'7645',\
                        "user_name":"Will","user_profile":username}
    md.update(icat_dict)

    #create the callback
    nx_publisher = NXWriterBESSY(_receivers=[icatreceiver()])
    pub_sub_id=RE.subscribe(nx_publisher.receiver)


    RE(scan([noisy_det],motor,1,2,10,md=md))

def test_nx_icat_csv_icat_nubes_receiver():

    #Generate csv and nx files. Upload both to Nubes, and the nx file also to ICAT

    load_dotenv()
    pw = os.environ.get("HZB_PASSWORD")
    username = os.environ.get("HZB_USERNAME")

    RE = RunEngine({})

    icat_dict = loginIcat(username, pw)
    
    md = {"sample":{"name":"test sample","formula":"SiO2"},\
                        "investigationid":'7645',\
                        "user_name":"Will","user_profile":username}
    md.update(icat_dict)

    #create nubes receiver and authenticate
    public_link = 'https://nubes.helmholtz-berlin.de/s/MJ6TKqDWG6iF9rD'
    public_pass = 'wqcysMYEmXeK4fN'
    
    meta_dict = {"public_link":public_link, "public_pass":public_pass, "nubes_path":"NubesTest/" }

    RE.md["Nubes"] = meta_dict #This contains the authorisation and where we want to send the data later
    


    #create the csv callback
    csv_publisher = CSVCallback(_receivers = [Nubesreceiver()])
    pub_sub_id=RE.subscribe(csv_publisher.receiver)

    #create the NX callback
    nx_publisher = NXWriterBESSY(_receivers=[icatreceiver(), Nubesreceiver()] )
    pub_sub_id=RE.subscribe(nx_publisher.receiver)


    RE(scan([noisy_det],motor,1,2,10,md=md))


def test_nx_csv_no_cred_receiver():

    #Check what happens if no credentials are provided in metadata

   
    RE = RunEngine({})

    
    #create the csv callback
    csv_publisher = CSVCallback(_receivers = [Nubesreceiver()])
    pub_sub_id=RE.subscribe(csv_publisher.receiver)

    #create the NX callback
    nx_publisher = NXWriterBESSY(_receivers=[icatreceiver(), Nubesreceiver()] )
    pub_sub_id=RE.subscribe(nx_publisher.receiver)


    RE(scan([noisy_det],motor,1,2,10))








    






