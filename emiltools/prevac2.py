#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 08:12:46 2022

@author: a2853
"""

import psycopg2
from emiltools.logger import log



class prevacDB():
    """
    Connects to the local Prevac Postgresql DB and gets stored information
    on samples
    """
    def getConnection(self):
        try:
            return psycopg2.connect("dbname='cluster' user='guest' host='sissy-data-01.exp.helmholtz-berlin.de' port='5432' password='guest'")
        except Exception as e:
            log.error("PREVAC: Could not connect to postgresql DB, Error: ",e)
            
    def request(self,query,values):
        conn = self.getConnection()
        cursor = conn.cursor()
        cursor.execute(query,values)
        des = cursor.description
        records = cursor.fetchall()
        conn.close()
        return records, des

    
    def getSampleInformation(self,id_sample):
        query = "select s.id_sample,s.name,s.description,sst.change_time,st.id_sampletype,st.name,st.description,sst.active,s.parent_id \
        from samples_tab s,sampletypes_tab st,samplestatus_tab sst where s.id_sample = %s and s.id_sampletype = st.id_sampletype \
        and sst.id_sample = s.id_sample;"
        values = (id_sample,)      
        return self.request(query,values)

    def getActualSampleInHolder(self,id_sample):
        query = "SELECT * FROM actual_samples_in_holder where id_sample = %s"
        values = (id_sample,)      
        return self.request(query,values)

    def getActualSamplesInHolder(self,id_holder):
       query = "SELECT * FROM actual_samples_in_holder where id_holder = %s"
       values = (id_holder,)      
       return self.request(query,values)