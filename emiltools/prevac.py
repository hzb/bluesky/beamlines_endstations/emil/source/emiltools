from __future__ import print_function
import psycopg2




def getHoldersInChamber(chamber):
    
    """
    Return a list of dicts containing information on the holders in a particular chamber
    
    chamber: string
        The name of the chamber like "ES"
        
    return
    
    holders: list of dict
        id           : string
        id_chamber   : string
        chamber_name : string
        id_holder    : int
        name         : string
        type_id      : string
        type         : string
        insert_time  : float
        eject_time   : float
        description  : string
    
    """

    conn = psycopg2.connect("dbname='cluster' user='guest' host='sissy-data-01.exp.helmholtz-berlin.de' port='5432' password='guest'")

    
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM actual_holders_in_chamber")
    records = cursor.fetchall()            
    conn.close()
    holders = []
    
    for record in records:
        if chamber in record:
          
            holders.append( {
                "id": record[0],
                "id_chamber": record[1],
                "chamber_name": record[2],
                "id_holder": record[4],
                "name": record[5],
                "type_id":record[6],
                "type":record[7],
                "insert_time": record[11].timestamp() if record[11] != None else None,
                "eject_time": record[12].timestamp() if record[12] != None else None,
                "description":record[14]

            })
    
    return(holders)
        
        
     
#getHoldersInChamber('ST1')   

def getSamplesInHolder(holder):
    
    """
    Return a list of dicts containing information on the samples for a particular holder like 'Holder10 (50x50)'
    
    holder: string
        The name of the holder like 'Holder10 (50x50)'
        
    return
    
    samples: list of dict
        id           : int
        name         : string
        description  : string
        id_holder    : int
        holder_name  : string
        insert_time  : float
        eject_time   : float
        parent_id    : int

    
    """
        
    conn = psycopg2.connect("dbname='cluster' user='guest' host='sissy-data-01.exp.helmholtz-berlin.de' port='5432' password='guest'")
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM actual_samples_in_holder")
    records = cursor.fetchall()
    conn.close()

    samples=[]
    for record in records:
            if holder in record:

                samples.append({
                "id": record[0],
                "name": record[1],
                "description": record[2],
                "id_holder": record[3],
                "holder_name": record[4],
                "insert_time": record[6].timestamp() if record[6] != None else None,
                "eject_time": record[7].timestamp() if record[7] != None else None,
                "parent_id": record[8]

                })
                
                
            
    return samples


def getSamplesInChamber(chamber):
    
    
    """
    Return a list of dicts containing information on the samples for a particular chamber like "ES"
    
    chamber: string
        The name of the chamber like 'ES'
        
    return
    
    samples: list of dict
        id           : int
        name         : string
        description  : string
        id_holder    : int
        holder_name  : string
        insert_time  : float
        eject_time   : float
        parent_id    : int

    
    """
        
        
    holders = getHoldersInChamber(chamber)
    samples = []
    for holder in holders:
        for sample in getSamplesInHolder(holder['name']):
            samples.append(sample)
    
    return samples

      

    