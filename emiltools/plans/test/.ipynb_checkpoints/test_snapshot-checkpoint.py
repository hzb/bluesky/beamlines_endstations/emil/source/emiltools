import pytest
from emiltools.plans.snapshot import snapshot



from bessyii_devices.sim import sim_motor

# Create a temporary RE and db
from bluesky.preprocessors import SupplementalData
from bluesky import RunEngine
from databroker.v2 import temp

## Set up env
RE = RunEngine({})
db = temp()
RE.subscribe(db.v1.insert)

# Create a baseline
sd = SupplementalData()

sd.baseline = [sim_motor] 
RE.preprocessors.append(sd)


def test_correct_length():


    plan = snapshot()
    length = sum(1 for _ in plan)
    
    assert length == 2
    

    
def test_baseline():
    
    #move the motor somewhere

    new_val = 5
    sim_motor.move(new_val)

    # Run the plan once 
    RE(snapshot())
    run = db[-1]
       
    #Check that the baseline contains the value (checks baseline exists...)
    pos = run.baseline.read()['sim_motor'].data[0]
    assert pos == new_val
    
def test_metadata():
    
      
    md_dict = {'name':'Will'}

    plan = snapshot(md = md_dict)
       
    # Run the plan once 
    RE(plan)
    run = db[-1]
    
    assert run.metadata['start']['name'] == 'Will'
       
    