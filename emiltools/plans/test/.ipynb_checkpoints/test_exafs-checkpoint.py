import pytest
import sys
import emiltools
from emiltools.plans.exafs_scan import exafs_scan
from ophyd.sim import det, noisy_det, motor
from bluesky import RunEngine

RE = RunEngine({})

#Get the databroker https://gist.github.com/untzag/53281819709b1058c7708a250cbd3676
from databroker.v2 import temp
db = temp()

# Insert all metadata/data captured into db.
RE.subscribe(db.v1.insert)


def single_region():
    RE(exafs_scan([det,noisy_det],motor, 1,-0.5,1,0.1))
    
    return db[-1]

def three_regions():
    RE(exafs_scan([det,noisy_det],motor, 3,-0.5,1,0.1,1,1.2,0.1,1.2,2,0.1))
    
    return db[-1]

#define tests
def test_single_region_runs():
    
    assert single_region().metadata['stop']['exit_status'] == 'success'
    

def test_multiple_regions_runs():
    
    assert three_regions().metadata['stop']['exit_status'] == 'success'


    