import time

from bluesky.plan_stubs import mv, abs_set
from bluesky.plans import count,scan, list_grid_scan, grid_scan
import numpy as np

from EMIL import user_ns as user_ns_module
user_ns = vars(user_ns_module)


##################
# switching beamline functions

def u17_switch_to_SISSY_I_PGM():
    #u17_closeshutter()   
    #yield from u17_U17_PGM()         
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_out()
    yield from u17_AU2()
    yield from u17_DCM_out_PGM_in()  
    ##u17_AUPINK_out()     # to be implemented
    ##u17_PINK_out()       # to be implemented
    yield from u17_AU3()            
    yield from u17_M3S_PGM()        
    yield from u17_slitS()
    u17_AU4S()
    yield from u17_M4SI_PGM()
    #u17_openvalves_SISSY()    
    #u17_openshutter()  


    print("\nExpected intensities:   -small AU1  (0.2 x 0.4): kth12/25/26 (diode(mesh)) 18 uA /  5 nA /  4 uA /")
    print("\n (@867.25 eV, 7 mm gap) -middle     (0.5 x 1.0):                          120 uA / 37 nA / 27 uA (0.8 nA)/")
    print("\n                        -large      (1.0 x 2.0):                          340 uA / 90 nA / 70 uA /")


def u17_switch_to_SISSY_I_DCM():

    u17_closeshutter()    
    yield from u17_U17_DCM()          
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_in()
    yield from u17_AU2()
    yield from u17_PGM_out_DCM_in()   
    #u17_AUPINK_out()      # to be implemented
    #u17_PINK_out()        # to be implemented
    yield from u17_AU3()              
    yield from u17_M3S_DCM()         
    yield from u17_slitS_out()
    u17_AU4S()
    yield from u17_M4SI_DCM()        
    #u17_openvalves_SISSY()    
    #u17_openshutter()    
    user_ns["kc_fs"](25,6)   
    print("\nkth25 set to 2.0e-6 uA fixed range")
    print("\nExpected intensities:    -small AU1 (0.2 x 0.4): kth12/25/26 (diode(mesh) 11 uA /  11 nA /  9 uA  /")
    print("\n (@5000 eV, 7.69 mm gap) -middle    (0.5 x 1.0):                          77 uA / 100 nA /  81 uA (0.5 nA)/")
    print("\n                         -large     (1.0 x 2.0):                         178 uA / 175 nA / 125 uA /")


def u17_switch_to_SISSY_II_PGM():
    #u17_closeshutter()     
    yield from u17_U17_PGM()
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_out()
    yield from u17_AU2()
    yield from u17_DCM_out_PGM_in()   
    #u17_AUPINK_out()       # to be implemented
    #u17_PINK_out()         # to be implemented
    yield from u17_AU3()
    yield from u17_M3S_PGM()
    yield from u17_slitS()
    u17_AU4S()
    yield from u17_M4SII_PGM()         
    #u17_openvalves_SISSY()      
    #u17_openshutter()  

         

def u17_switch_to_SISSY_II_DCM():
    #u17_closeshutter()    
    yield from u17_U17_DCM()
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_in()
    yield from u17_AU2()
    yield from u17_PGM_out_DCM_in()   
    #u17_AUPINK_out()      # to be implemented
    #u17_PINK_out()        # to be implemented
    yield from u17_AU3()
    yield from u17_M3S_DCM()          
    yield from u17_slitS_out() 
    u17_AU4S()
    yield from u17_M4SII_DCM()        
    #u17_openvalves_SISSY()    
    #u17_openshutter()
    user_ns["kc_fs"](25,6)     
    print("kth25 set to 2.0e-6 uA fixed range")
    
def u17_switch_to_CAT_PGM():
    #u17_closeshutter()    
    yield from u17_U17_PGM()
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_out()
    yield from u17_AU2()
    yield from u17_DCM_out_PGM_in()  
    #u17_AUPINK_out()      # to be implemented
    #u17_PINK_out()        # to be implemented
    yield from u17_AU3()
    yield from u17_M3C_PGM()          
    u17_AU4C()
    u17_M4C_PGM() # reintroduce yield from when problem is solved
    #u17_openvalves_CAT()     
    #u17_openshutter()     

    print("\nExpected intensities:   -small AU1  (0.2 x 0.4): kth14/27/28:  25 uA /  11 nA /  10 uA /")
    print("\n (@867.25 eV, 7 mm gap) -middle     (0.5 x 1.0):              183 uA /  70 nA /  70 uA /")
    print("\n                        -large      (1.0 x 2.0):              470 uA / 250 nA / 240 uA /")


def u17_switch_to_CAT_DCM():
    u17_closeshutter()      
    yield from u17_pinhole()
    yield from u17_AU1_middle()
    u17_M1()   # change back to yield from when problem is solved
    yield from u17_foil_in()
    yield from u17_AU2()
    yield from u17_PGM_out_DCM_in()    
    #u17_AUPINK_out()         # to be implemented
    #u17_PINK_out()           # to be implemented  
    yield from u17_AU3()
    yield from u17_M3C_DCM()
    yield from u17_slitC_out()
    u17_AU4C()
    u17_M4C_DCM() # reintroduce yield from when problem is solved
    #u17_openvalves_CAT()
    #u17_openshutter()
    user_ns["kc_fs"](27,7)       
    yield from fstart_Mostab0()          

    print("kth27 set to 2.0e-7 uA fixed range")
    print("Expected intensities:  -small AU1 (0.2 x 0.4): kth14/27/28: 3 uA /  11 nA /  10 uA /")
    print("(@5000 V, 7.69 mm gap) -middle   (0.5 x 1.0):             17 uA /  77 nA /  70 uA /")
    print("                       -large     (1.0 x 2.0):             34 uA / 148 nA / 140 uA /")
    print("beam at CAT-Io fluorescent screen edge at 13.6 mm (long hole pos.: 18.8 (r) - 23.0 (l) mm)/")

def u17_switch_to_PINK_PINK():
    u17_closeshutter
    u17_U17_home
    u17_pinhole
    u17_AU1_middle
    u17_M1C
    u17_foil_in
    u17_AU2
    u17_DCM_out_PGM_out
    u17_AUPINK_in
    u17_PINK_in
    #u17_openvalvesP
    u17_openshutter
    #u17_openshutterP

def u17_switch_to_PINK_DCM():
    u17_closeshutter
    u17_U17_DCM
    u17_pinhole
    u17_AU1_middle
    u17_M1C
    u17_foil_in
    u17_AU2
    u17_PGM_out_DCM_in
    u17_AUPINK_out
    u17_PINK_in
    #u17_openvalvesP
    u17_openshutter
    #u17_openshutterP
    #fstart_Mostab2




#######################
# Auxiliary functions #
#######################

############################################################################
# UNDULATOR

def u17_U17_PGM():
    print("\nI move U17-gap to 7.0 mm (corresponding to 867.25 eV (Ne 1s) in 1st harmonic) ")
    u17_IDoff()
    user_ns["u17"].gap.put(7.0)
    user_ns["u17"].cmd_exec.put(1)

def u17_U17_DCM():
    print("\nI move U17-gap 7.629 mm (corresponding to 5 keV in 5th Harmonic)")
    u17_IDoff()
    user_ns["u17"].gap.put(7.629)
    user_ns["u17"].cmd_exec.put(1)


def u17_U17_home():
    print("\nI move U17-gap to a safe position of 10 mm ")
    u17_IDoff()
    user_ns["u17"].gap.put(10.0)
    user_ns["u17"].gap.cmd_exec.put(1)

############################################################################
# PINHOLE

def u17_pinhole():
    print("\nI move frontend pinholes to home position ")
    yield from mv(
        user_ns["ph"].h, 0,
        user_ns["ph"].v, -0.25)


############################################################################
# APERTURES

def u17_AU1_small():
    print("\nI move frontend apertures AU1 to 0.4 x 0.2 mm (h x v) opening ")
    yield from mv(user_ns["u17_au1"].top, 0.850,
          user_ns["u17_au1"].bottom, 0.650,
          user_ns["u17_au1"].right, -0.110, # electrons
          user_ns["u17_au1"].left, 0.290) # wall


def u17_AU1_middle():
    print("\nI move frontend apertures AU1 to 1.0 x 0.5 mm (h x v) opening ")
    yield from mv(user_ns["u17_au1"].top, 1.0,
          user_ns["u17_au1"].bottom, 0.5,
          user_ns["u17_au1"].right, -0.410, # electrons
          user_ns["u17_au1"].left, 0.590) # wall
          


def u17_AU1_large():
    print("\nI move frontend apertures AU1 to 2.0 x 1.0 mm (h x v) opening ")
    yield from mv(user_ns["u17_au1"].top, 1.250,
          user_ns["u17_au1"].bottom, 0.250,
          user_ns["u17_au1"].right, -0.910, # electrons
          user_ns["u17_au1"].left, 1.09) # wall
          

def u17_AU2():
    # ok
    print("\nI move monochromator apertures AU2 to 2.6 x 2.6 mm (h x w) opening ")
    yield from mv(user_ns["u17_au2"].top, 2.15,
          user_ns["u17_au2"].bottom, -0.35,
          user_ns["u17_au2"].right, 1.75, 
          user_ns["u17_au2"].left, -0.85) 
          
    
def u17_AU3():
   print("FIND OUT REAL VALUES TO SET HERE. 1.6 FOR LEFT WAS ABOVE THE LIMIT.")
   print("\nI move M3-mirror apertures to 3.7 x 0.3 mm (h x w)")
   yield from mv(user_ns["u17_au3"].top, 3.1,
         user_ns["u17_au3"].bottom, -1.6,
         user_ns["u17_au3"].right, -1.0,
         user_ns["u17_au3"].left, -1.9)
    
def u17_AU4S():
    print("Aperture unit AU4-SISSY should be at position: 44/23/22/15 mm (T/B/R/L)")
    print("This is only a message, AU4 are not motorized")
 

def u17_AU4C():
    print("\nAperture unit AU4-CAT should be at position: 41/19.5/13.5/20.5 mm (T/B/R/L)")
    print("This is only a message, AU4 are not motorized")


############################################################################
# FOIL

dict_foil_choice={1:'Diamond foil 5 um', 2:'Diamond foil 10 um', 3:'Diamond foil 20 um'}

def u17_foil_choice_v(device_readback):
    if device_readback != 0:
        print(f'\nThe selected dimaond foil in vertical direction is: {dict_foil_choice[device_readback]}') 
    else:
        print(f'\nNo diamond foil selected!') 
        
def u17_foil_choice_h(device_readback):
    if device_readback != 0:
        print(f'\nThe selected dimaond foil in horizontal direction is: {dict_foil_choice[device_readback]}') 
    else:
        print(f'\nNo diamond foil selected!') 

def u17_foil_in():    
    print("\nMoving diamond foil into beam")  
    u17_foil_choice_v(user_ns["u17_df"].v.choice_diamand.readback.get())
    u17_foil_choice_h(user_ns["u17_df"].h.choice_diamand.readback.get())    
    yield from mv(user_ns["u17_df"].v.move, 38)
    yield from mv(user_ns["u17_df"].h.move, 0)
       
def u17_foil_out():
    print("\nMoving diamond foil to home position")
    print("\nMoving diamond foil into beam")  
    u17_foil_choice_v(user_ns["u17_df"].v.choice_diamand.readback.get())
    u17_foil_choice_h(user_ns["u17_df"].h.choice_diamand.readback.get())    
    yield from mv(user_ns["u17_df"].v.move, 90)
    yield from mv(user_ns["u17_df"].h.move, 0)




############################################################################
# M1 MIRROR
#def u17_M1():
#    print("\nI set mirror M1 ")
#    yield from mv(user_ns["u17_m1"].t_x, 570)
#    yield from mv(user_ns["u17_m1"].t_y, 500)
#    yield from mv(user_ns["u17_m1"].r_x, -1390)
#    yield from mv(user_ns["u17_m1"].r_y, -10649)
#    yield from mv(user_ns["u17_m1"].r_z, 10500)

# I redefine the function because the implementation above does not work

def u17_M1():
    print("\nI set mirror M1 ")
    user_ns["u17_m1"].tx.set(570)
    time.sleep(5)
    user_ns["u17_m1"].ty.set(500)
    time.sleep(5)
    user_ns["u17_m1"].rx.set(-1390)
    time.sleep(5)
    user_ns["u17_m1"].ry.set(-10650)
    time.sleep(5)
    user_ns["u17_m1"].rz.set(10500)
    time.sleep(10)


############################################################################
# M3 MIRRORS

def u17_M3S_PGM():
    print("I move M3-mirror to SISSY-position")
    yield from mv(user_ns["u17_m3"].start_immediately, 0)
    yield from mv(user_ns["u17_m3"].choice, 'SISSY')

    yield from mv(user_ns["u17_m3"].start_immediately, 1)
    user_ns["u17_m3"].tx.set(4140)
    user_ns["u17_m3"].ty.set(520)
    user_ns["u17_m3"].tz.set(0)
    user_ns["u17_m3"].rx.set(-160)
    user_ns["u17_m3"].ry.set(350)

    yield from mv(user_ns["u17_m3"].start_immediately, 0)
    yield from mv(user_ns["u17_m3"].rz, -4000)

       
 
def u17_M3S_DCM():
    print("I move M3-mirror to SISSY-position")
    yield from mv(user_ns["u17_m3"].start_immediately, 0)
    yield from mv(user_ns["u17_m3"].choice, 'SISSY')

    yield from mv(user_ns["u17_m3"].start_immediately, 1)
    user_ns["u17_m3"].tx.set(4140)
    user_ns["u17_m3"].ty.set(520)
    user_ns["u17_m3"].tz.set(0)
    user_ns["u17_m3"].rx.set(-260)
    user_ns["u17_m3"].ry.set(800)
    yield from mv(user_ns["u17_m3"].start_immediately, 0) 
    yield from mv( user_ns["u17_m3"].rz, -4000)


def u17_M3C_PGM():
    print("I move M3-mirror to SISSY-position")
    yield from mv(user_ns["u17_m3"].start_immediately, 0)
    yield from mv(user_ns["u17_m3"].choice, 'CAT')

    yield from mv(user_ns["u17_m3"].start_immediately, 1)
    user_ns["u17_m3"].tx.set(-2250)
    user_ns["u17_m3"].ty.set(-220)
    user_ns["u17_m3"].tz.set(0)
    user_ns["u17_m3"].rx.set(-102)
    user_ns["u17_m3"].ry.set(550)
    yield from mv(user_ns["u17_m3"].start_immediately, 0) 
    yield from mv( user_ns["u17_m3"].rz, -7100)


def u17_M3C_DCM():
    print("\nI move M3-mirror to CAT-position")
    yield from mv(user_ns["u17_m3"].start_immediately, 0)
    yield from mv(user_ns["u17_m3"].choice, 'CAT')

    yield from mv(user_ns["u17_m3"].start_immediately, 1)
    user_ns["u17_m3"].tx.set(-2250)
    user_ns["u17_m3"].ty.set(-220)
    user_ns["u17_m3"].tz.set(0)
    user_ns["u17_m3"].rx.set(102)
    user_ns["u17_m3"].ry.set(550)
    yield from mv(user_ns["u17_m3"].start_immediately, 0) 
    yield from mv( user_ns["u17_m3"].rz, -7100)


############################################################################
# M4 MIRRORS
def u17_M4SI_PGM():
    print("\nI move M4-SISSY-I mirror")
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0)
    yield from mv(user_ns["u17_m4_sissy"].choice, 'SISSY I')
    
    print('Now I set the positions')
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 1)
    user_ns["u17_m4_sissy"].tx.set(1300)
    user_ns["u17_m4_sissy"].ty.set(-220)
    user_ns["u17_m4_sissy"].tz.set(0)
    user_ns["u17_m4_sissy"].rx.set(-400)
    user_ns["u17_m4_sissy"].ry.set(-500)
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0) 
    yield from mv(user_ns["u17_m4_sissy"].rz, 0)


def u17_M4SI_DCM():
    print("\nI move M4-SISSY-I mirror")
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0)
    yield from mv(user_ns["u17_m4_sissy"].choice, 'SISSY I')

    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 1)
    user_ns["u17_m4_sissy"].tx.set(-1250)
    user_ns["u17_m4_sissy"].ty.set(-2500)
    user_ns["u17_m4_sissy"].tz.set(0)
    user_ns["u17_m4_sissy"].rx.set(-500)
    user_ns["u17_m4_sissy"].ry.set(-2100)
    yield from mv(user_ns["u17_m4_sissy"].start_immediately,0) 
    yield from mv(user_ns["u17_m4_sissy"].rz, -3000)


def u17_M4SII_PGM():
    print("\nI move M4-SISSY-II mirror")
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0)
    yield from mv(user_ns["u17_m4_sissy"].choice, 'SISSY II')

    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 1)
    user_ns["u17_m4_sissy"].tx.set(450)
    user_ns["u17_m4_sissy"].ty.set(1850)
    user_ns["u17_m4_sissy"].tz.set(0)
    user_ns["u17_m4_sissy"].rx.set(235)
    user_ns["u17_m4_sissy"].ry.set(-2300)
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0) 
    yield from mv( user_ns["u17_m4_sissy"].rz, 700)

def u17_M4SII_DCM():
    print("\nI move M4-SISSY-II mirror")
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0)
    yield from mv(user_ns["u17_m4_sissy"].choice, 'SISSY II')
    print('I switched')
    time.sleep(10)
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 1)
    user_ns["u17_m4_sissy"].tx.set(450)
    user_ns["u17_m4_sissy"].ty.set(1850)
    user_ns["u17_m4_sissy"].tz.set(0)
    user_ns["u17_m4_sissy"].rx.set(235)
    user_ns["u17_m4_sissy"].ry.set(-2300)
    print('I changed the values but the last one')
    time.sleep(20)
    yield from mv(user_ns["u17_m4_sissy"].start_immediately, 0) 
    yield from mv(user_ns["u17_m4_sissy"].rz, 700)


def u17_M4C_PGM():
    print("\nI move M4-mirror")
    yield from mv(user_ns["u17_m4_cat"].start_immediately, 1)
    user_ns["u17_m4_cat"].tx.set(3270)
    user_ns["u17_m4_cat"].ty.set(5720)
    user_ns["u17_m4_cat"].tz.set(0)
    user_ns["u17_m4_cat"].rx.set(1000)
    user_ns["u17_m4_cat"].ry.set(1200)
    yield from mv(user_ns["u17_m4_cat"].start_immediately, 0) 
    yield from mv(user_ns["u17_m4_cat"].rz, 3400)

def u17_M4C_DCM():
    print("\nI move M4-mirror")
    yield from mv(user_ns["u17_m4_cat"].start_immediately, 1)
    user_ns["u17_m4_cat"].tx.set(3310)
    user_ns["u17_m4_cat"].ty.set(5550)
    user_ns["u17_m4_cat"].tz.set(0)
    user_ns["u17_m4_cat"].rx.set(-1000)
    user_ns["u17_m4_cat"].ry.set(-1200)
    yield from mv(user_ns["u17_m4_cat"].start_immediately, 0) 
    yield from mv(user_ns["u17_m4_cat"].rz, -3400)



############################################################################
# SLITS

def u17_slitS():
    print("\nI move exit slit SISSY to standard opening (100 um)")
    print("\nHorizontal apertures should be at position 4/10 mm (L/R)")
    yield from mv(user_ns["u17_es_sissy"], 100) 

def u17_slitS_out():
    print("\nI open exit slit SISSY (2600 um)")
    print("\nHorizontal apertures should be at position 4/10 mm (L/R)")
    yield from mv(user_ns["u17_es_sissy"], 2600) 

def u17_slitC():
    print("\nI move exit slit CAT to standard opening (100 um)")
    print("\nHorizontal apertures should be at position 19/14.9 mm (L/R)")
    yield from mv(user_ns["u17_es_cat"], 100)

def u17_slitC_out():
    print("\nI open exit slit CAT (2600 um)")
    print("\nHorizontal apertures should be at position 19/14.9 mm (L/R)")
    yield from mv(user_ns["u17_es_cat"], 2600)

###########################################################################
# IDon and IDoff

def u17_IDon():
    user_ns["u17_pgm"].ID_on.put(1) # 1 should set IDon
    user_ns["u17_dcm"].ID_on.put(1) 
    user_ns["u17"].id_control.put(1) # 1 should set to remote
    user_ns["u17_pgm"].table.put(0)
    user_ns["u17_dcm"].table.put(0)
    #user_ns["u17_pgm"].harmonic.put(0) # 1st harmonic
    time.sleep(1)
    user_ns["u17_dcm"].harmonic.put(7) # maxflux

def u17_IDoff():
    user_ns["u17_pgm"].ID_on.put(0) # 1 should set IDoff
    user_ns["u17_dcm"].ID_on.put(0)
    user_ns["u17"].id_control.put(0) # 1 should set to local

# Separate for DCM and PGM
# PGM 
def u17_IDon_PGM():
    user_ns["u17_pgm"].ID_on.put(1) # 1 should set IDon
    user_ns["u17"].id_control.put(1) # 1 should set to remote
    user_ns["u17_pgm"].table.put(0)
    #user_ns["u17_pgm"].harmonic.put(0) # 1st harmonic
    time.sleep(1)

def u17_IDoff_PGM():
    user_ns["u17_pgm"].ID_on.put(0) # 1 should set IDoff
    user_ns["u17"].id_control.put(0) # 1 should set to local
   
# DCM

def u17_IDon_DCM():
    user_ns["u17_dcm"].ID_on.put(1)  # 1 should set IDoff
    user_ns["u17"].id_control.put(1) # 1 should set to remote
    user_ns["u17_dcm"].table.put(0)
    #user_ns["u17_pgm"].harmonic.put(0) # 1st harmonic
    time.sleep(1)
    user_ns["u17_dcm"].harmonic.put(7) # maxflux

def u17_IDoff_DCM():
    user_ns["u17_dcm"].ID_on.put(0) # 1 should set IDoff
    user_ns["u17"].id_control.put(0) # 1 should set to local

###########################################################################
# Channel-cut on and off

def u17_channelcut_on():
    user_ns["u17_dcm"].channelcut.set(1) # 1 should set cc on

def u17_channelcut_off():
    user_ns["u17_dcm"].channelcut.set(0) # 0 should set channelcut off

############################################################################
# PGM and DCM

def u17_PGM_out_DCM_in():
    print("\nI move DCM-crystal to Si 111 position (108) and set mono to 5 keV")
    print("\nI move PGM-mirror and 800 l/mm-grating to home position (770/60)")
    u17_channelcut_off()
    user_ns["u17_dcm"].crystal.set("111")
    yield from mv(
        user_ns["u17_dcm"].ct1, 108,
        user_ns["u17_pgm"].m2_translation, 770,
        user_ns["u17_pgm"].grating_translation, 60)
    u17_IDon()
    yield from mv(user_ns["u17_dcm"], 3150)
    user_ns["u17_dcm"].piezo.r_height.setpoint.set(2500)


def u17_DCM_out_PGM_out():
    print("\nI move DCM-crystal to home position (0.5)")
    print("\nI move PGM-mirror and 800 l/mm-grating to home position (770/60)")
    yield from mv(
        user_ns["u17_dcm"].ct1, 0.5,
        user_ns["u17_pgm"].m2_translation, 770,
        user_ns["u17_pgm"].grating_translation, 60)
    user_ns["u17_dcm"].crystal.set("Space")
    print('Mostab was not stopped, function not yet implemented')

def u17_DCM_out_PGM_in():
    print("\nI move DCM-crystal to home position (0.5)")
    print("\nI move PGM-mirror, 800 l/mm-grating into beam (721/165) and set mono to 867.25 eV (Ne 1s)")
    yield from mv(
        user_ns["u17_dcm"].ct1, 0.5,
        user_ns["u17_pgm"].m2_translation, 721, 
        user_ns["u17_pgm"].grating_translation, 165,
        user_ns["u17_pgm"].cff, 2.25,
        user_ns["u17_pgm"], 1200)
    user_ns["u17_dcm"].crystal.put("Space")
    print("Mostab was not stopped, function not yet implemented")
 

def u17_PGM_out():
    print("\nI move PGM-mirror and 800 l/mm-grating to home position (770/60)")
    yield from mv(
        user_ns["u17_pgm"].m2_translation, 770,
        user_ns["u17_pgm"].grating_translation, 60)
    

############################################################################
# MOSTAB

def start_Mostab0():
    print('attention: probably this function only toggles mostab')
    yield from mv(
        user_ns["mostab_0"].manual_mode_cmd, 1,
        user_ns["mostab_0"].peak_hold_mode_cmd, 1)
    
def stop_Mostab0():
    print('attention: probably this function only toggles mostab')
    yield from mv(
        user_ns["mostab_0"].manual_mode_cmd, 1)
      
############################################################################
# VALVES

u17_valves_shared = ['u17_valve4','u17_valve5','u17_valve6']
u17_valves_shared = np.array(u17_valves_shared)

u17_valves_cat = ['valve3','u17_valve4','u17_valve5','u17_valve6',
                  'u17_valve7_sissy_cat','u17_valve8_sissy_cat',                                         'u17_valve9_cat','u17_valve10_cat','u17_valve11_cat','u17_valve12_cat',
                  'u17_valve13_cat']
u17_valves_cat = np.array(u17_valves_cat)

u17_valves_sissy=['valve3','u17_valve4','u17_valve5','u17_valve6',
                  'u17_valve7_sissy_cat','u17_valve8_sissy_cat', 
                  'u17_valve9_sissy','u17_valve10_sissy','u17_valve11_sissy']
u17_valves_sissy = np.array(u17_valves_sissy)

##################
## Status valves
def u17_statusvalves_CAT():
    print("The status of the valves at CAT is:")
    for valve in u17_valves_cat:
        print(valve,": ",user_ns[valve].readback.get())


def u17_statusvalves_SISSY():
    print("The status of the valves at SISSY is:")
    for valve in u17_valves_sissy:
        print(valve,": ",user_ns[valve].readback.get())

def u17_statusvalves_shared():
    print("The status of the shared valves is:")
    for valve in u17_valves_shared:
        print(valve,": ",user_ns[valve].readback.get())

##################
### Open/Close
# redefined in u17 switch beamline file
def close_valve(valve):
    if user_ns[valve].readback.get() == "opened(SFO-)":
            if valve == 'valve3':
                user_ns[valve].setpoint.set('1')
            else:
                user_ns[valve].setpoint.set('toggle')

def open_valve(valve):
    if user_ns[valve].readback.get() == "closed(-F-C)":
            if valve == 'valve3':
                user_ns[valve].setpoint.set('1')
            else:
                user_ns[valve].setpoint.set('toggle')
######################
### Open/Close valve 3
def closevalve3():
    print("\nI close all valves in SISSY-beamline")
    if user_ns["valve3"].readback.get() == "opened(SFO-)":
        user_ns["valve3"].setpoint.set('toggle')
    time.sleep(0.1)
    u17_valvesSISSY()

##########
### SISSY
def u17_openvalves_SISSY():
    print("\nI open all valves in SISSY-beamline")
    for x in range(1):
        for valve in np.flip(u17_valves_sissy):
            if valve=='u17_valve4':
                continue
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    u17_statusvalves_SISSY()
    print("valve 4 is not changed because is broken. If this is not the case anymore ask Simone Vadilonga to change this")
    
def u17_closevalves_SISSY():
    print("\nI close all valves in SISSY-beamline")
    for x in range(3):
        for valve in u17_valves_sissy:
            if valve == 'valve3' or valve in u17_valves_shared:
                continue
            close_valve(valve)
        time.sleep(0.5)
    u17_statusvalves_SISSY()
    print("The status of valve 3 was not changed. \nIf you close it you may toggle the beamshutter and interrupt measurements on soft branches.")
    print("To close valve 3 use closevalve3()")
    print("valve 4 is broken, move it from blue panels")

##########
### CAT
def u17_openvalves_CAT():
    print("\nI open all valves in CAT-beamline")
    for x in range(1):
        for valve in np.flip(u17_valves_cat):
            if valve == 'u17_valve4':
                continue
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    u17_statusvalves_CAT()
    print("valve 4 is broken, move it from blue panels. If the info is outdated contact Simone Vadilonga or Sebastian Kazarski")

def u17_closevalves_CAT():
    print("\nI close all valves in CAT-beamline")
    for x in range(3):
        for valve in u17_valves_cat:
            if valve == "valve3" or valve in u17_valves_shared:
                continue
            close_valve(valve)
        time.sleep(0.5)
    u17_statusvalves_CAT()
    print("The status of valve 3 was not changed. \nIf you close it you may toggle the beamshutter and interrupt measurements on soft branches.")
    print("To close valve 3 use closevalve3()")
    print("valve 4 is broken, move it from blue panels. to change this beahviour ask Simone Vadilonga or Sebastian Kazarski")

############
### SHARED
def u17_openvalves_shared():
    print("\nI open all the shared valves")
    for x in range(1):
        for valve in np.flip(u17_valves_shared):
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    u17_statusvalves_shared()

def u17_closevalves_shared():
    print("\nI close all the shared valves")
    for x in range(3):
        for valve in u17_valves_shared:
            close_valve(valve)
            time.sleep(0.5)
    u17_statusvalves_shared()

##############################################################################
### UNDULATOR GAP
def u17_move_gap(gap_value):
    user_ns["u17"].gap.put(gap_value)
    user_ns["u17"].cmd_exec.put(1)
    time.sleep(5)

############################################################################
# SHUTTER

def u17_closeshutter():
    if user_ns["u17_ps"].readback.get() != "closed(---C)" and user_ns["u17_ps"].readback.get() != 'closed(-F-C)':
        user_ns["u17_ps"].set('1')
    time.sleep(3)
    print("U17-photon shutter status: ", user_ns["u17_ps"].readback.get())
        
def u17_openshutter():

    if user_ns["u17_ps"].readback.get() == "closed(---C)" or user_ns["u17_ps"].readback.get() == 'closed(-F-C)':
        user_ns["u17_ps"].set('1')
    time.sleep(3)
    print("U17-photon shutter status: ", user_ns["u17_ps"].readback.get())




############################################################################
# PINK
# implemented 251021 
def u17_AUPINK_in():
    print("\nI move PINK-mirror apertures to 1.2 x 1.6 mm (h x w) opening")
    yield from mv(user_ns["u17_au3_pink"].top, -19.4,
          user_ns["u17_au3_pink"].bottom, -20.6,
          user_ns["u17_au3_pink"].right, -0.9, # electrons
          user_ns["u17_au3_pink"].left, -2.5) # wall


def u17_AUPINK_out():
    print("\nI move PINK-mirror apertures to 5.0 x 3.4 mm (h x w) opening")
    yield from mv(user_ns["u17_au3_pink"].top, 3.5,
          user_ns["u17_au3_pink"].bottom, -1.5,
          user_ns["u17_au3_pink"].right, 0, # electrons
          user_ns["u17_au3_pink"].left, -3.4) # wall

    
def u17_PINK_in():
    print("\nI move PINK-mirror into beam")
    yield from mv(user_ns["u17_m2_pink"].start_immediately, 0) 
    yield from mv(user_ns["u17_m2_pink"].ty, 0)
    

def u17_PINK_out():
    print("\nI move PINK-mirror to home position")
    yield from mv(user_ns["u17_m2_pink"].start_immediately, 0) 
    yield from mv(user_ns["u17_m2_pink"].ty, -4000)    

###########################################################################
# Set Table and Harmonic

def u17_setPGMHarmonic(harmonic):
    """
    Change the harmonic for the U17 undulator
    
    Parameters
    ----------
    harmonic : int or str
        1 for the first harmonic
        3 for the second harmonic
        5 for the fifth harmonic
        7 for the seventh harmonic
        9 for the nineth harmonic
        11 for the enleventh harmonic
        13 for the thirtienth harmonic
        'maxflux' for maxflux
    """

    d={1:0,3:1,5:2,7:3,9:4,11:5,13:6,'maxflux':7}    
    if harmonic not in d:
        print("Value not permitted.")
        print("Permitted values are", d.keys())
    else:
        user_ns["u17_pgm"].harmonic.put(d[harmonic])


def u17_setDCMHarmonic(harmonic):
    """
    Change the harmonic for the U17 undulator
    
    Parameters
    ----------
    harmonic : int or str
        1 for the first harmonic
        3 for the second harmonic
        5 for the fifth harmonic
        7 for the seventh harmonic
        9 for the nineth harmonic
        11 for the enleventh harmonic
        13 for the thirtienth harmonic
        'maxflux' for maxflux
    """

    d={1:0,3:1,5:2,7:3,9:4,11:5,13:6,'maxflux':7}    
    if harmonic not in d:
        print("Value not permitted.")
        print("Permitted values are", d.keys())
    else:
        user_ns["u17_dcm"].harmonic.put(d[harmonic])
    

def u17_setPGMTable():
    """
    Set the table for the U17 undulator for PGM operations
    """
    user_ns["u17_pgm"].table.put(0)
    # future structure when more tables will be needed
    #d={0:0}
    #if table in d:
    #    user_ns["u17_pgm"].table.put(d[table])
    #elif isinstance(table,int)==True:
    #    if table >=0 or table <= 11:
    #        user_ns["u17_pgm"].table.put(d[table])
    #    else:
    #        print("Permitted Values are:")
    #        print("str: ", d.keys())
    #        print("int: values between [0,11]")

def u17_setDCMTable():
    """
    Set the table for the U17 undulator for DCM operations
    """
    user_ns["u17_dcm"].table.put(0)
    # future structure when more tables will be needed
    #d={0:0}
    #if table in d:
    #    user_ns["u17_pgm"].table.put(d[table])
    #elif isinstance(table,int)==True:
    #    if table >=0 or table <= 11:
    #        user_ns["u17_pgm"].table.put(d[table])
    #    else:
    #        print("Permitted Values are:")
    #        print("str: ", d.keys())
    #        print("int: values between [0,11]")

    

