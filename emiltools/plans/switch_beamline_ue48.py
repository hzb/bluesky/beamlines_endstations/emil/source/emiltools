from bluesky.plan_stubs import mv
import numpy as np
from EMIL import user_ns as user_ns_module
user_ns = vars(user_ns_module)

import time

def ue48_switch_to_CAT():
    """
    Switch ue48_m3 Mirror to CAT
    Select CAT branch on ue48_pgm
    Reset the energy
    """
    print('Changing M3 to CAT')
    yield from mv(user_ns["ue48_m3"].choice,      'CAT',
          user_ns["ue48_pgm"].set_branch, 'CAT')
    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)          
    print("M3-SMU is on CAT position")



def ue48_switch_to_SISSYI():
    """
    Switch ue48_m3 Mirror to SISSY
    Switch ue48_m4 mirror to SISSYI
    Select SISSYI branch on ue48_pgm
    Reset the energy
    """
    print('Changing M3 to SISSY and M4 to SISSY-I')
    yield from mv(user_ns["ue48_m3"].choice,      'SISSY',
          user_ns["ue48_m4_sissy"].choice,'SISSY-I',
          user_ns["ue48_pgm"].set_branch, 'SISSY')
    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)          
    print("M3-SMU and M4-SISSY is on SISSY-I position")



def ue48_switch_to_SISSYII():
    """
    Switch ue48_m3 Mirror to SISSY
    Switch ue48_m4 mirror to SISSYII
    Select SISSYII branch on ue48_pgm
    Reset the energy
    """

    print('Changing M3 to SISSY and M4 to SISSY-II')
    yield from mv(user_ns["ue48_m3"].choice,      'SISSY',
          user_ns["ue48_m4_sissy"].choice,'SISSY-II',
          user_ns["ue48_pgm"].set_branch, 'SISSY')
    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)          
    print("M3-SMU and M4-SISSY is on SISSY-II position")



def ue48_M3_to_SISSY():
    """
    Switch ue48_m3 Mirror to SISSY
    Select SISSY branch on ue48_pgm
    Reset the energy
    """
    print('Changing M3 to SISSY')
    yield from mv(user_ns["ue48_m3"].choice,      'SISSY',
          user_ns["ue48_pgm"].set_branch, 'SISSY')
    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)          
    print("M3-SMU is on SISSY position")

    
def ue48_switch_to_STXM():
    """
    Switch ue48_m3 Mirror to STXM
    Select STXM branch on ue48_pgm
    Reset the energy
    """
    print('Changing M3 to STXM')
    yield from mv(user_ns["ue48_m3"].choice,      'PEEM', # this is not a mistake! 
          user_ns["ue48_pgm"].set_branch, 'STXM')
    print('Resetting the energy')    
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)   
    print("M3-SMU is on STXM position")
    

############################################################
# valves

ue48_valves_shared = ['ue48_valve4','ue48_valve5','ue48_valve6']
ue48_valves_shared = np.array(ue48_valves_shared)

ue48_valves_sissy = ['valve3','ue48_valve4','ue48_valve5','ue48_valve6',
                  'ue48_valve7_sissy','ue48_valve8_sissy', 
                  'ue48_valve9_sissy','ue48_valve10_sissy','ue48_valve11_sissy']
ue48_valves_sissy=np.array(ue48_valves_sissy)

ue48_valves_cat = ['valve3','ue48_valve4','ue48_valve5','ue48_valve6',
                  'ue48_valve7_cat','ue48_valve8_cat','ue48_valve9_cat','ue48_valve10_cat',
                   'ue48_valve11_cat','ue48_valve12_cat','ue48_valve13_cat']
ue48_valves_cat=np.array(ue48_valves_cat)

ue48_valves_stxm = ['valve3','ue48_valve4','ue48_valve5','ue48_valve6',
                  'ue48_valve7_stxm','ue48_valve8_stxm', 
                  'ue48_valve9_stxm' ,'ue48_valve11_stxm']  # dont open valve 11 automatically
ue48_valves_stxm=np.array(ue48_valves_stxm)

##################
## Status valves
def ue48_statusvalves_CAT():
    print("The status of the valves at CAT is:")
    for valve in ue48_valves_cat:
        print(valve,": ",user_ns[valve].readback.get())


def ue48_statusvalves_SISSY():
    print("The status of the valves at SISSY is:")
    for valve in ue48_valves_sissy:
        print(valve,": ",user_ns[valve].readback.get())

def ue48_statusvalves_STXM():
    print("The status of the valves at STXM is:")
    for valve in ue48_valves_stxm:
        print(valve,": ",user_ns[valve].readback.get())

def ue48_statusvalves_shared():
    print("The status of the shared valve is:")
    for valve in ue48_valves_shared:
        print(valve,": ",user_ns[valve].readback.get())

##################
### Open/Close
# redefined in u17 switch beamline file
def close_valve(valve):
    if user_ns[valve].readback.get() == "opened(SFO-)":
            if valve == 'valve3':
                user_ns[valve].setpoint.set('1')
            else:
                user_ns[valve].setpoint.set('toggle')

def open_valve(valve):
    if user_ns[valve].readback.get() == "closed(-F-C)":
            if valve == 'valve3':
                user_ns[valve].setpoint.set('1')
            else:
                user_ns[valve].setpoint.set('toggle')
######################
### Open/Close valve 3
def closevalve3():
    print("\nI close all valves in SISSY-beamline")
    if user_ns["valve3"].readback.get() == "opened(SFO-)":
        user_ns["valve3"].setpoint.set('toggle')
    time.sleep(0.1)
    ue48_statusvalves_SISSY()

##########
### SISSY
def ue48_openvalves_SISSY():
    print("\nI open all valves in SISSY-beamline")
    for x in range(1):
        for valve in np.flip(ue48_valves_sissy):
            if valve=='u17_valve4':
                continue
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    ue48_statusvalves_SISSY()

def ue48_closevalves_SISSY():
    print("\nI close all valves in SISSY-beamline")
    for x in range(3):
        for valve in ue48_valves_sissy:
            if valve == 'valve3' or valve in ue48_valves_shared:
                continue
            close_valve(valve)
        time.sleep(0.5)
    ue48_statusvalves_SISSY()
    print("The status of valve 3 was not changed. \nIf you close it you may toggle the beamshutter and interrupt measurements on other branches.")
    print("To close valve 3 use closevalve3()")

##########
### CAT
def ue48_openvalves_CAT():
    print("\nI open all valves in CAT-beamline")
    for x in range(1):
        for valve in np.flip(ue48_valves_cat):
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    ue48_statusvalves_CAT()

def ue48_closevalves_CAT():
    print("\nI close all valves in CAT-beamline")
    for x in range(3):
        for n in ue48_valves_cat:
            if valve == "valve3" or valve in ue48_valves_shared:
                continue
            close_valve(valve)
        time.sleep(0.5)
    ue48_statusvalves_CAT()
    print("The status of valve 3 was not changed. \nIf you close it you may toggle the beamshutter and interrupt measurements on other branches.")
    print("To close valve 3 use closevalve3()")

##########
### STXM
def ue48_openvalves_STXM():
    print("\nI open all valves in STXM-beamline")
    for x in range(1):
        for valve in np.flip(ue48_valves_stxm):
            if valve == 'ue48_valve11_stxm':
                print("I don't open valve 11. Ask SV to change this if necessary.")
                continue
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    ue48_statusvalves_STXM()

def ue48_closevalves_STXM():
    print("\nI close all valves in STXM-beamline")
    for x in range(3):
        for valve in ue48_valves_stxm or valve in ue48_valves_shared:
            if valve == "valve3" or valve=='ue48_valve4' or valve=='ue48_valve5' or valve=='ue48_valve6':
                continue
            close_valve(valve)
            time.sleep(0.5)
    ue48_statusvalves_CAT()
    print("The status of valve 3 was not changed. \nIf you close it you may toggle the beamshutter and interrupt measurements on other branches")
    print("To close valve 3 use closevalve3()")

############
### SHARED
def ue48_openvalves_shared():
    print("\nI open all the shared valves")
    for x in range(1):
        for valve in np.flip(ue48_valves_shared):
            open_valve(valve)
            time.sleep(1)
    time.sleep(5)
    ue48_statusvalves_shared()

def ue48_closevalves_shared():
    print("\nI close all the shared valves")
    for x in range(3):
        for valve in ue48_valves_shared:
            close_valve(valve)
            time.sleep(0.5)
    ue48_statusvalves_shared()

############################################################################
# SHUTTER


def ue48_closeshutter():
    
    #fix to make this be a plan for so that it's a generator https://github.com/bluesky/bluesky-queueserver/blob/2d20ca3bd4185a3a6775e046e720673328cadbfc/bluesky_queueserver/manager/profile_ops.py#L404
    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)  

    if user_ns["ue48_ps"].readback.get() != "closed(---C)" and user_ns["ue48_ps"].readback.get() != 'closed(-F-C)':
        user_ns["ue48_ps"].set('1')
    time.sleep(3)
    print("Ue48-photon shutter status: ", user_ns["ue48_ps"].readback.get())

def ue48_openshutter():

    print('Resetting the energy')
    energy=user_ns["ue48_pgm"].readback.get()
    yield from mv(user_ns["ue48_pgm"], energy)  

    if user_ns["ue48_ps"].readback.get() == "closed(---C)" or user_ns["ue48_ps"].readback.get() == 'closed(-F-C)':
        user_ns["ue48_ps"].set('1')
    time.sleep(3)
    print("Ue48-photon shutter status: ", user_ns["ue48_ps"].readback.get())


############################################################################
# UNDULATOR GAP

def ue48_gap(gap_value):
    user_ns["ue48"].gap.set(gap_value)
    yield from mv(user_ns["ue48"].cmd_exec,1)


###########################################################################
# IDon and IDoff

def ue48_IDon():
    user_ns["ue48_pgm"].ID_on.set(1) # 1 should set IDon
    user_ns["ue48"].id_control.set(1) # 1 should set to remote
    #if set_table != False:
    #    user_ns["ue48_pgm"].table.put(set_table)
    #user_ns["ue48_pgm"].harmonic.put(4)

def ue48_IDoff():
    user_ns["ue48_pgm"].ID_on.set(0) # 1 should set IDoff
    user_ns["ue48"].id_control.set(0) # 1 should set to local


###########################################################################
# Set Table and Harmonic

def ue48_setHarmonic(harmonic):
    """
    Change the harmonic for the UE48 undulator
    
    Parameters
    ----------
    harmonic : int or str
        1 for the first harmonic
        3 for the second harmonic
        5 for the fifth harmonic
        7 for the seventh harmonic
        'maxflux' for maxflux
    """

    d={1:0,3:1,5:2,7:3,'maxflux':4}    
    if harmonic not in d:
        print("Value not permitted.")
        print("Permitted values are", d.keys())
    else:
        user_ns["ue48_pgm"].harmonic.put(d[harmonic])
    

def ue48_setTable(table):
    """
    Change the table for the UE48 undulator
    
    Parameters
    ----------
    table : int or str 
        "EllipPos" 
        "EllipNeg"
        "LinHor"
        "LinVer"
        an integer between [0,11] for the other tables,
        see BluePanels for the options
    """

    d={"EllipPos":0,"EllipNeg":1,"LinHor":2,"LinVer":3}
    if table in d:
        user_ns["ue48_pgm"].table.put(d[table])
    elif isinstance(table,int)==True:
        if table >=0 or table <= 11:
            user_ns["ue48_pgm"].table.put(d[table])
        else:
            print("Permitted Values are:")
            print("str: ", d.keys())
            print("int: values between [0,11]")

