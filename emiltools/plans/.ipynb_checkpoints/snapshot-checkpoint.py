# A plan to do nothing but open and close


from bluesky.plan_stubs import open_run, close_run

def snapshot(md=None):
    
    """
    A plan which only opens and closes with nothing in between. 
    When used with a supplimental data preporcessor a baseline will be taken
    In this way we can take dated snapshots of the machine
    
    """
    
    _md = {'plan_name': 'snapshot',
           'hints': {}
           }
    _md.update(md or {})
    
    rs_uid = yield from open_run(_md)
    yield from close_run(exit_status =  'success', reason = 'complete')

    
    return rs_uid

def gold_snapshot(md=None):
    
    """
    A plan which only opens and closes with nothing in between. 
    When used with a supplimental data preporcessor a baseline will be taken
    In this way we can take dated snapshots of the machine
    
    """
    
    _md = {'plan_name': 'gold_snapshot',
           'hints': {}
           }
    _md.update(md or {})
    
    rs_uid = yield from open_run(_md)
    yield from close_run(exit_status =  'success', reason = 'complete')

    
    return rs_uid



