import math
from bluesky.utils import snake_cyclers
from itertools import chain, zip_longest
from cycler import cycler

try:
    # cytools is a drop-in replacement for toolz, implemented in Cython
    from cytools import partition
except ImportError:
    from toolz import partition
from collections import defaultdict  

from bluesky import plan_patterns

from bluesky import utils
from bluesky.utils import Msg

from bluesky import preprocessors as bpp
from bluesky import plan_stubs as bps

import numpy as np

def closestDivisors(n):
    a = round(math.sqrt(n))
    while n%a > 0: a -= 1
    return [a,n//a]


def scan_nd_shutter(detectors, cycler,shutter, *, md=None):
    """
    Scan over an arbitrary N-dimensional trajectory. open a shutter every time we take a measurement

    Parameters
    ----------
    detectors : list
    cycler : Cycler
        cycler.Cycler object mapping movable interfaces to positions
    shutter : motor
    
        1 to open 0 to close

    md : dict, optional
        metadata

    See Also
    --------
    :func:`bluesky.plans.inner_product_scan`
    :func:`bluesky.plans.grid_scan`

    Examples
    --------
    >>> from cycler import cycler
    >>> cy = cycler(motor1, [1, 2, 3]) * cycler(motor2, [4, 5, 6])
    >>> scan_nd([sensor], cy)
    """
    _md = {'detectors': [det.name for det in detectors],
           'motors': [motor.name for motor in cycler.keys],
           'shutter':shutter.name,
           'num_points': len(cycler),
           'num_intervals': len(cycler) - 1,
           'plan_args': {'detectors': list(map(repr, detectors)),
                         'cycler': repr(cycler)},
           'plan_name': 'scan_nd',
           'hints': {},
           }
    _md.update(md or {})
    try:
        dimensions = [(motor.hints['fields'], 'primary')
                      for motor in cycler.keys]
    except (AttributeError, KeyError):
        # Not all motors provide a 'fields' hint, so we have to skip it.
        pass
    else:
        # We know that hints exists. Either:
        #  - the user passed it in and we are extending it
        #  - the user did not pass it in and we got the default {}
        # If the user supplied hints includes a dimension entry, do not
        # change it, else set it to the one generated above
        _md['hints'].setdefault('dimensions', dimensions)

   

       
    pos_cache = defaultdict(lambda: None)  # where last position is stashed
    cycler = utils.merge_cycler(cycler)
    motors = list(cycler.keys)

    @bpp.stage_decorator(list(detectors) + motors)
    @bpp.run_decorator(md=_md)
    def inner_scan_nd():
        for step in list(cycler):
            motors = step.keys()
            yield from bps.move_per_step(step, pos_cache)
            yield from bps.abs_set(shutter, 1, wait=True)
            yield from bps.trigger_and_read(list(detectors) + list(motors)+ list([shutter]))
            yield from bps.abs_set(shutter, 0, wait=True)


    return (yield from inner_scan_nd())




def rad_reduction_scan(detectors, *args, num=None,motor_x=None,x_range=None, motor_y=None, y_range=None,shutter=None, md=None):
    """
    Scan over one multi-motor trajectory.

    Parameters
    ----------
    detectors : list
        list of 'readable' objects
    *args :
        For one dimension, ``motor, start, stop``.
        In general:

        .. code-block:: python

            motor1, start1, stop1,
            motor2, start2, start2,
            ...,
            motorN, startN, stopN

        Motors can be any 'settable' object (motor, temp controller, etc.)
    num : integer
        number of points
        
    x_motor : motor
    
    x_range : list
        
        range over which to move X
    
    y_motor : motor
    
    y_range : list
        
        range over which to move Y
    
    shutter : Motor
    
        writing 1 to this will open it, 0 to close
        
    md : dict, optional
        metadata

    See Also
    --------
    :func:`bluesky.plans.relative_inner_product_scan`
    :func:`bluesky.plans.grid_scan`
    :func:`bluesky.plans.scan_nd`
    """
    # For back-compat reasons, we accept 'num' as the last positional argument:
    # scan(detectors, motor, -1, 1, 3)
    # or by keyword:
    # scan(detectors, motor, -1, 1, num=3)
    # ... which requires some special processing.
    if num is None:
        if len(args) % 3 != 1:
            raise ValueError("The number of points to scan must be provided "
                             "as the last positional argument or as keyword "
                             "argument 'num'.")
        num = args[-1]
        args = args[:-1]

    if not (float(num).is_integer() and num > 0.0):
        raise ValueError(f"The parameter `num` is expected to be a number of "
                         f"steps (not step size!) It must therefore be a "
                         f"whole number. The given value was {num}.")
    num = int(num)

    md_args = list(chain(*((repr(motor), start, stop)
                           for motor, start, stop in partition(3, args))))
    motor_names = tuple(motor.name for motor, start, stop
                        in partition(3, args))
    md = md or {}
    _md = {'plan_args': {'detectors': list(map(repr, detectors)),
                         'num': num, 'args': md_args},
           'plan_name': 'scan',
           'plan_pattern': 'inner_product',
           'plan_pattern_module': plan_patterns.__name__,
           'plan_pattern_args': dict(num=num, args=md_args),
           'motors': motor_names
           }
    _md.update(md)

    sizes = closestDivisors(num)
    
    traj1 = cycler(motor_x, np.linspace(x_range[0],x_range[1],sizes[0]))
    traj2 = cycler(motor_y, np.linspace(y_range[0],y_range[1],sizes[1]))
    snake = snake_cyclers([traj1, traj2],[False,True])
 
    snake = snake +  plan_patterns.inner_product(num=num, args=args)



    # get hints for best effort callback
    motors = [motor for motor, start, stop in partition(3, args)]
    
    if motor_x != None:
        motors.append(motor_x)
        motors.append(motor_y)
        motors.append(shutter)
        
    # Give a hint that the motors all lie along the same axis
    # [(['motor1', 'motor2', ...], 'primary'), ] is 1D (this case)
    # [ ('motor1', 'primary'), ('motor2', 'primary'), ... ] is 2D for example
    # call x_fields because these are meant to be the x (independent) axis
    x_fields = []
    for motor in motors:
        x_fields.extend(getattr(motor, 'hints', {}).get('fields', []))

    default_dimensions = [(x_fields, 'primary')]

    default_hints = {}
    if len(x_fields) > 0:
        default_hints.update(dimensions=default_dimensions)

    # now add default_hints and override any hints from the original md (if
    # exists)
    _md['hints'] = default_hints
    _md['hints'].update(md.get('hints', {}) or {})

    
    return (yield from scan_nd_shutter(detectors, snake,
                               md=_md, shutter=shutter))
